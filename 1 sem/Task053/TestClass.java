package Task053;

import Task050.RationalFraction;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {
        RationalVector2D rv = new RationalVector2D();
        RationalFraction rf = new RationalFraction(2,6);
        RationalFraction rf1 = new RationalFraction(3,7);
        RationalVector2D rv1 = new RationalVector2D(rf,rf1);
        RationalVector2D add = rv1.add(rv1);
        System.out.println(add.toString());
        System.out.println(add.lenght());
        System.out.println(rv1.scalarProduct(rv1));
        System.out.println(add.equals(rv1));
        System.out.println(add.equals(add));
    }
}
