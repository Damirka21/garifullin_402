package Task053;

import Task049.Vector2D;
import Task050.RationalFraction;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class RationalVector2D {
    public RationalFraction getRf2() {
        return rf2;
    }

    public String toString(){
        return ("<" + rf1.toString() + ";" + rf2.toString() + ">") ;
    }

    public void setRf2(RationalFraction rf2) {
        this.rf2 = rf2;
    }

    public RationalFraction getRf1() {
        return rf1;
    }

    public void setRf1(RationalFraction rf1) {
        this.rf1 = rf1;
    }

    private RationalFraction rf1;
    private RationalFraction rf2;
    public RationalVector2D(RationalFraction r1,RationalFraction r2) {
        rf1 = r1;
        rf2 = r2;
    }
    public RationalVector2D(){
        this(new RationalFraction(),new RationalFraction());
    }
    public RationalVector2D add(RationalVector2D r){
        RationalVector2D add = new RationalVector2D();
        add.rf1 = this.getRf1().add(r.getRf1()) ;
        add.rf2 = this.getRf2().add(r.getRf2()) ;
        return add;
    }
    public double lenght(){
        double len = (1.0 *(rf1.getX()*rf1.getX()) / (rf1.getY()*rf1.getY())) + (1.0 *(rf2.getX()*rf2.getX()) / (rf2.getY()*rf2.getY()));
        return len;
    }
    public RationalFraction scalarProduct(RationalVector2D r){
        RationalFraction sc = new RationalFraction();
        RationalFraction sc1 = new RationalFraction();
        sc = this.rf1.mult(r.getRf1());
        sc1 = this.rf2.mult(r.getRf2());
        sc = sc.add(sc1);
        return sc;
    }
    public boolean equals(RationalVector2D r){
        if (this.rf1.equals(r.rf1) && this.rf2.equals(r.rf2)) {
            return true;
        } else {
            return false;
        }
    }
}
