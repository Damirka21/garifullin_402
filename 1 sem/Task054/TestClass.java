package Task054;

import Task051.ComplexNumber;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {

        ComplexNumber cn1 = new ComplexNumber(1,0);
        ComplexNumber cn2 = new ComplexNumber(1,1);
        ComplexVector2D cv = new ComplexVector2D(cn1,cn2);
        ComplexVector2D cv1 = new ComplexVector2D(cn2,cn1);
        ComplexVector2D add = cv.add(cv1);
        System.out.println(cv.toString());
        System.out.println(add.toString());
        ComplexNumber sc = cv.scalarProduct(cv1);
        System.out.println(sc.toString());
        System.out.println(cv.equals(cv));
        System.out.println(cv.equals(cv1));
}
}

