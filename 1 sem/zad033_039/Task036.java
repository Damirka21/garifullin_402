package zad033_039;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         036
 */
public class Task036 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int j = a[0];
        for (int i = 1; i < n; i++) {
            if (a[i] > j) {
                System.out.println(a[i]);
                j = a[i];
            }
        }
    }
}
