package zad033_039;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         037
 */
public class Task037 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        for (int j = 0; j < n / 2; j++) {
            for (int i = j + 1; i < n - j - 1; i++) {
                a[j][i] = 0;
                a[n - j - 1][n - i - 1] = 0;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j]);
            }
            System.out.println();
        }
    }
}
