package zad033_039;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         039
 */
public class Task039 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размеры матрицы");
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int[][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = scanner.nextInt();
            }
        }
        System.out.println("Введите кол-во чисел");
        int p = scanner.nextInt();
        int[] b = new int[p];
        for (int i = 0; i < p; i++) {
            b[i] = scanner.nextInt();
        }
        //конец чтения данных
        boolean[] bool = new boolean[p];//есть число или нет

        for (int i = 0; i < p; i++){
            System.out.print(b[i] + " ");
            int k1,k2;
            int help = b[i];
            int kol = 0;
            do {
                help = help / 10 ;
                kol = kol + 1;
            } while(help > 0); // кол-во цифр
            int[] cif = new int[kol];
            help = b[i];
            for (int j = kol - 1; j >= 0; j -- ){
                cif[i] = help % 10;
                help = help / 10;
            } // запомиание числа по цифрам
            int help1 = 0;
            int j1 = 0; int i1=0;
            do {
                j1++;
                if (j1 >= p){
                    i1++;
                    j1 = 0;
                }
                k1 = i1 ;
                k2 = j1 ;
            } while((a[i1][j1] == cif[0]) && (i1 < p )); //запоминание кординаты 1 цифры
            do {
                help ++;
                j1 = j1 + 1;
                if (a[i1][j1] == b[help]){
                    bool[i] = true;
                }else {
                    bool[i] = false;
                };
            } while (help < kol && bool[i] == true);
        }
    }
}
