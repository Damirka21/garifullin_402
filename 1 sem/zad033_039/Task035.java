package zad033_039;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         035
 */
public class Task035 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new  int[n];
        for (int i = 0; i < n; i ++){
            a[i] = scanner.nextInt();
        }
        for (int i = 0; i < n -1; i ++){
            for ( int j = i + 1; j < n; j++ ){
                if (a[i] > a [j]) {
                    int v = a[i];
                    a[i] = a [j];
                    a[j] = v;
                }
            }
        }
        for (int i= 0; i < n; i++){
            System.out.print(a[i]);
        }
    }
}
