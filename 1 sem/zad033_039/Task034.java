package zad033_039;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         034
 */
public class Task034 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int max = 0;
        for (int i = 0; i < n - 2; i++) {
            int sum = 0;
            for (int j = i; j < i + 3; j++) {
                sum = sum + a[j];
            }
            if (sum > max) {
                max = sum;
            }
        }
        System.out.println(max);
    }
}
