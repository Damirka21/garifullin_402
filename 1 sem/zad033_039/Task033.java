package zad033_039;

import javafx.scene.input.PickResult;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         033
 */
public class Task033 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }
        long pro = 0;
        double ad = 0, bd = 0;
        for (int i = 0; i < n; i++) {
            pro = pro + a[i] * b[i];
            ad = a[i] * a[i] + ad;
            bd = b[i] * b[i] + bd;
        }
        ad = Math.sqrt(ad);
        bd = Math.sqrt(bd);
        double u = Math.abs(pro) / (ad * bd);
        double x = Math.acos(u) / Math.PI * 180;
        System.out.println(pro);
        System.out.println(x + "degree");
    }
}
