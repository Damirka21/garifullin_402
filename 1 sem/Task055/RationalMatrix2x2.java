package Task055;

import Task050.RationalFraction;
import Task053.RationalVector2D;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class RationalMatrix2x2 {

    public String toString(){
        for (int i = 0; i < 2; i ++){
            for (int j = 0; j < 2; j ++){
                System.out.print(this.a[i][j] + "\t");
            }
            System.out.println();
        }
        return "";
    }

    public RationalFraction[][] getA() {
        return a;
    }

    public void setA(RationalFraction[][] a) {
        this.a = a;
    }

    private RationalFraction[][] a = new RationalFraction[2][2];

    public RationalMatrix2x2(RationalFraction[][] rat) {
        for (int i = 0; i < 2; i ++){
            for (int j = 0; j < 2; j ++){
                this.a[i][j] = rat[i][j];
            }
        }
    }
    public RationalMatrix2x2(){
        for (int i = 0; i < 2; i ++){
            for (int j = 0; j < 2; j ++){
                this.a[i][j] = new RationalFraction();
            }
        }
    }

    public RationalMatrix2x2(RationalFraction r1,RationalFraction r2,RationalFraction r3,RationalFraction r4){
        this.a[0][0] = r1;
        this.a[0][1] = r2;
        this.a[1][0] = r3;
        this.a[1][1] = r4;
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 b) {
        RationalMatrix2x2 add = new RationalMatrix2x2();
        for (int i = 0; i < 2; i ++){
            for (int j = 0; j < 2; j ++){
                add.a[i][j] = this.a[i][j].add(b.a[i][j]);
            }
        }
        return add;
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 b){
        RationalMatrix2x2 mult = new RationalMatrix2x2();
        mult.a[0][0] = (this.a[0][0].mult(b.a[0][0])).add(this.a[0][1].mult(b.a[1][0]));
        mult.a[1][0] = (this.a[1][0].mult(b.a[0][0])).add(this.a[1][1].mult(b.a[1][0]));
        mult.a[0][1] = (this.a[0][0].mult(b.a[0][1])).add(this.a[0][1].mult(b.a[1][1]));
        mult.a[1][1] = (this.a[1][0].mult(b.a[0][1])).add(this.a[1][1].mult(b.a[1][1]));
        return mult;
    }

    public RationalVector2D multVector(RationalVector2D b){
        RationalFraction c = this.a[0][0].mult(b.getRf1()).add(this.a[0][1].mult(b.getRf2()));
        RationalFraction d = this.a[1][0].mult(b.getRf1()).add(this.a[1][1].mult(b.getRf2()));
        return new RationalVector2D(c,d);
    }

    public RationalFraction det(){
        RationalFraction c =  this.a[1][0].div(this.a[0][0]);
        for (int k = 0; k < 2; k++){
            this.a[1][k] = a[1][k].sub(this.a[0][k].mult(c));
        }
        c = this.a[1][1].mult(this.a[0][0]);
        return c;
    }
}
