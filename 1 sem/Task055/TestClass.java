package Task055;

import Task050.RationalFraction;
import Task053.RationalVector2D;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {
        RationalVector2D vec = new RationalVector2D();
        RationalMatrix2x2 rm1 = new RationalMatrix2x2();
        RationalFraction rf1 = new RationalFraction(2, 5);
        RationalFraction rf2 = new RationalFraction(1, 3);
        RationalFraction rf3 = new RationalFraction(5, 7);
        RationalFraction rf4 = new RationalFraction(3, 4);
        RationalFraction[][] arr = {{rf3, rf2}, {rf1, rf4}};
        RationalMatrix2x2 rm2 = new RationalMatrix2x2(arr);
        RationalMatrix2x2 rm3 = new RationalMatrix2x2(rf1, rf2, rf3, rf4);
        System.out.println(rm1.toString());
        System.out.println(rm2.toString());
        System.out.println(rm3.toString());
        System.out.println(rm2.add(rm3));
        System.out.println(rm2.mult(rm3));
        System.out.println(rm2.multVector(vec));
        System.out.println(rm2.det());
    }
}
