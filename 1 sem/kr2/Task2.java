package kr2;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int z = 0;
        int[][] a = new int[n][n];
        int i = n - 1;
        while (z < n *n) {
            for (int j = n - 1; j >= 0; j--) {
                a[i][j] = z;
                z++;
            }
            i--;
            if (i < 0) {
                break;
            }
            for (int j = 0; j < n; j++) {
                a[i][j] = z;
                z++;
            }
            i--;


        }
        for (i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
