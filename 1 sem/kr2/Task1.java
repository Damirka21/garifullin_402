package kr2;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for ( int i= 0; i < n; i ++){
            a[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i ++) {
            int j =i;
            int c;
            if (a[j] != -1){
                System.out.println();
            }
            while (a[j] != -1)
            {
                c = a[j];
                System.out.print(a[j]);
                a[j] = -1;
                j = c;
            }

        }
    }
}
