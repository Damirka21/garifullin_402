package Task052;

import Task049.Vector2D;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {
        Vector2D ve = new Vector2D(1,0);
        double[][] b = {{2,0},{0,2}};
        double[][] c = {{1,2,3},{2,2,3},{3,2,1}};
        Matrix2x2 m1 = new Matrix2x2();
        Matrix2x2 m0 = new Matrix2x2(c);
        Matrix2x2 m2 = new Matrix2x2(b);
        Matrix2x2 m3 = new Matrix2x2(2.5);
        Matrix2x2 m4 = new Matrix2x2(3,5,4,2);
        System.out.println(m1.toString());
        System.out.println(m2.toString());
        System.out.println(m3.toString());
        System.out.println(m4.toString());
        System.out.println(m0.toString());
        System.out.println(m3.add(m4));
        System.out.println(m3.sub(m4));
        m3.add2(m4);
        System.out.println(m3.toString());
        m3.sub2(m4);
        System.out.println(m4.toString());
        System.out.println(m2.multNumber(2));
        m2.multNumber2(2);
        System.out.println(m2.toString());
        m4.transpon();
        System.out.println(m4.toString());
        System.out.println(m4.equivalentDIagonal());
        System.out.println(m4.det());
        Matrix2x2 inv = m2.inverseMatrix();
        System.out.println(inv.toString());
        System.out.println(m2.mult(inv));
    }
}
