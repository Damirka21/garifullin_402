package Task050;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {
        RationalFraction rationalFraction = new RationalFraction(-231,99);
        RationalFraction rationalFraction1 = new RationalFraction(88,-33);
        rationalFraction.reduce();
        RationalFraction add = rationalFraction.add(rationalFraction1);
        RationalFraction sub = rationalFraction.sub(rationalFraction1);
        rationalFraction.add2(rationalFraction1);
        System.out.println(rationalFraction.toString());
        rationalFraction.sub2(rationalFraction1);
        System.out.println(add.toString());
        System.out.println(sub.toString());
        System.out.println(rationalFraction.toString());
        RationalFraction mult = rationalFraction.mult(add);
        System.out.println(mult.toString());
        rationalFraction.mult2(add);
        System.out.println(rationalFraction.toString());
        RationalFraction div = rationalFraction.div(rationalFraction1);
        System.out.println(div.toString());
        rationalFraction.div2(rationalFraction1);
        System.out.println(rationalFraction.toString());
        double c = rationalFraction.value();
        System.out.println(c);
        System.out.println(rationalFraction.equals(rationalFraction1));
        System.out.println(rationalFraction.equals(rationalFraction));
        int a = rationalFraction.numberPart();
        System.out.println(a);
    }


}
