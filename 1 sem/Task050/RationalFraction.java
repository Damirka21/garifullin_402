package Task050;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class RationalFraction {
    private int x;
    private int y;

    public int numberPart() {
        int c = this.x / this.y;
        return c;
    }

    public boolean equals(RationalFraction rationalFraction) {
        this.reduce();
        rationalFraction.reduce();
        return this.x == rationalFraction.x && this.y == rationalFraction.y;
    }

    public double value() {
        double c = 1.0 * this.x / this.y;
        return c;
    }

    public void div2(RationalFraction rationalFraction) {
        this.x = rationalFraction.y * this.x;
        this.y = rationalFraction.x * this.y;
        this.reduce();
    }

    public RationalFraction div(RationalFraction rationalFraction) {
        RationalFraction div = new RationalFraction(this.x * rationalFraction.y, this.y * rationalFraction.x);
        div.reduce();
        return div;
    }

    public void mult2(RationalFraction r) {
        this.x = this.x * r.getX();
        this.y = this.y * r.getY();
        this.reduce();
    }

    public RationalFraction mult(RationalFraction r) {
        RationalFraction mult = new RationalFraction(this.x * r.getX(), this.y * r.getY());
        mult.reduce();
        return mult;
    }

    public void sub2(RationalFraction rationalFraction) {
        this.x = this.x * rationalFraction.y - rationalFraction.x * this.y;
        this.y = this.y * rationalFraction.y;
        this.reduce();
    }

    public RationalFraction sub(RationalFraction rationalFraction) {
        RationalFraction subed = new RationalFraction();
        subed.x = this.x * rationalFraction.y - rationalFraction.x * this.y;
        subed.y = this.y * rationalFraction.y;
        subed.reduce();
        return subed;
    }

    public void add2(RationalFraction rationalFraction) {
        this.x = this.x * rationalFraction.y + rationalFraction.x * this.y;
        this.y = this.y * rationalFraction.y;
        this.reduce();
    }

    public RationalFraction add(RationalFraction rationalFraction) {
        RationalFraction added = new RationalFraction();
        added.x = this.x * rationalFraction.y + rationalFraction.x * this.y;
        added.y = this.y * rationalFraction.y;
        added.reduce();
        return added;
    }

    public String toString() {
        if (x < 0) {
            return (" - " + (-1)*x + "/" + y );
        }else{
            return (" + " + x + "/" + y);
        }
    }

    public void reduce() {
        int i = 2;
        while (i <= this.y) {
            if (this.x % i == 0 && this.y % i == 0) {
                this.x = this.x / i;
                this.y = this.y / i;
                i = 1;
            }
            i++;
        }
        if (y<0){
            y *= -1;
            x *= -1;
        }
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public RationalFraction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public RationalFraction() {
        this(0, 0);
    }
}
