package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 011
 */

import java.util.Scanner;

public class Task011 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        long factorial = 1;
        int i;
        for (i = n; i > 0; i = i - 2) {
                factorial = factorial * i;
            }
        System.out.println(factorial);
    }
}
