package zad009_022;

/**
 * @author Damir Garifullin
 * 11-402
 * 022
 */

import java.util.Scanner;

public class Task022 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i, j;
        for (i = 0; i <= (2 * n); i++) {
            for (j = 0; j <= (2 * n); j++) {
                if ((i - n) * (i - n) + (j - n) * (j - n) <= n * n) {
                    System.out.print('0');
                } else {
                    System.out.print('*');
                }
            }
            System.out.println();
        }
    }
}
