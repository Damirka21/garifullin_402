package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 013
 */

import java.util.Scanner;

public class Task013 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double r = 1;
        int i;
        int n = scanner.nextInt();
        for (i = 1; i <= n; i++) {
            r = r * 2 * i / (2 * i - 1) * 2 * i / (2 * i + 1);
        }
        System.out.println(r);
    }
}
