package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 009
 */

import java.util.Scanner;

public class Task009 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        double y;
        if (x > 2) {
            y = (x * x - 1) / (x + 2);
        } else {
            if (x > 0) {
                y = (x * x - 1) * (x + 2);
            } else {
                y = x * x * (1 + 2 * x);
            }
        }
        System.out.println(y);
    }
}