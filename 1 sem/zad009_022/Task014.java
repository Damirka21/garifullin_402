package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 014
 */

import java.util.Scanner;

public class Task014 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i;
        double x = scanner.nextDouble();
        double cos = x;
        for (i = 1; i <= n; i++) {
            cos = x + Math.cos(cos);
        }
        System.out.println(cos);
    }
}
