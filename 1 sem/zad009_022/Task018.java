package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 018
 */

import java.util.Scanner;

public class Task018 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        int n = scanner.nextInt();
        double sum = 0;
        int i;
        for (i = 1; i <= n + 1; i++) {
            double a = scanner.nextDouble();
            sum = sum * x + a;
        }
        System.out.println(sum);
    }
}
