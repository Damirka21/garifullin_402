package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 010
 */

import java.util.Scanner;

public class Task010 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        double y;
        y = x > 2 ? (x * x - 1) / (x + 2) : (x > 0 ? (x * x - 1) * (x + 2) : x * x * (1 + 2 * x));
        System.out.println(y);
    }
}
