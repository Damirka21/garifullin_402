package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 017
 */

import java.util.Scanner;

public class Task017 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m;
        double s = 0.5; // первое слагаемое
        double drob = s;
        for (m = 2; m <= n; m++) {
            drob = drob * (m - 1) * (m - 1) / ((2 * m - 1) * 2 * m);
            s = s + drob;
        }
        System.out.println(s);
    }
}
