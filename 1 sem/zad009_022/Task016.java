package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 016
 */

import java.util.Scanner;

public class Task016 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double x = scanner.nextDouble();
        double s = 0;
        double y = 1;
        int i;
        for (i = 1; i <= n; i++) {
            y = y * (x + i);
            s = s + y;
        }
        System.out.println(s);
    }
}
