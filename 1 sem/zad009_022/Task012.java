package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 012
 */

import java.util.Scanner;

public class Task012 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i, j;
        double S = 0;
        int n = scanner.nextInt();
        int znak = -1 ; //чтоб 1 слагаемое было положительным
        for (i = 1; i <= 2 * n - 1; i = i + 2) {
            znak = znak * - 1;
            S = S + znak * (1.0 / (i * i));
        }
        System.out.println(S);
    }
}
