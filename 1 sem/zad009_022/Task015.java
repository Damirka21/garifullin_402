package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 015
 */

import java.util.Scanner;

public class Task015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double x = scanner.nextDouble();
        int i;
        double s = n + x;
        for (i = n; i >= 2; i--) {
            s = (i - 1) + x / s;
        }
        System.out.println(s);
    }
}
