package zad09_22;

/**
 * @author Damir Garifullin
 * 11-402
 * 019
 */


public class Task019 {
    public static void main(String[] args) {
        int i, j, sum;
        for (i = 1; i <= 1000000; i++) {
            sum = 0;
            for (j = 1; j < i; j++) {
                if (i % j == 0) {
                    sum = sum + j;
                }
            }
            if (i == sum) {
                System.out.println(i);
            }
        }
    }
}
