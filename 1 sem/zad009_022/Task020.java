package zad009_022;

/**
 * @author Damir Garifullin
 * 11-402
 * 020
 */

import java.util.Scanner;

public class Task020 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i, j;
        for (i = 1; i <= n + 1; i++) {
            for (j = 1; j <= (n + 1 - i); j++) {
                System.out.print("*");
            }
            for (j = n + 2 - i; j <= n + i; j++) {
                System.out.print("0");
            }
            for (j = n + i + 1; j <= 2 * n + 1; j++){
                System.out.print("*");
            }
                System.out.println();
        }
        for (i = 1; i <= n ; i++) {
            for (j = 1; j <= i; j++) {
                System.out.print("*");
            }
            for (j = i + 1; j <= 2 * n + 1 - i; j ++){
                System.out.print("0");
            }
            for (j = 2 * n + 1 - i; j <= 2 * n ; j ++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
