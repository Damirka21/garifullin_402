package zad044_046;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         044
 */
public class Task044 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        String[] array = a.split(" ");
        for (int i = 0; i < array.length; i++) {
            boolean p1 = true;
            if (array[i].charAt(0) >= 'A' && array[i].charAt(0) <= 'Z') {
                int j = 1;
                do {
                    if ((array[i].charAt(j)) <= 'z' && (array[i].charAt(j)) >= 'a') {
                        p1 = true;
                    } else {
                        p1 = false;
                    }
                    j++;
                } while (p1 && (j < array[i].length()));
                if (p1) {
                    System.out.print(array[i] + " ");
                }
            }

        }
    }
}
