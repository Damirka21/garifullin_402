package zad044_046;

import java.awt.*;
import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task046 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        String b = scanner.nextLine();
        int n;
        if (a.length() > b.length()) {
            n = b.length();
        } else {
            n = a.length();
        }
        char a1, b1;
        int i = 0;
        do {
            a1 = a.charAt(i);
            b1 = b.charAt(i);
            i++;
        } while (i < n && a1 == b1);
        if (a1 > b1) {
            System.out.println(b);
            System.out.println(a);
        } else {

            System.out.println(a);
            System.out.println(b);
        }
    }
}
