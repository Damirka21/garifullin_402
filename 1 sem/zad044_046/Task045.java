package zad044_046;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         045
 */
public class Task045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Кол-во команд:");
        int n = scanner.nextInt();
        System.out.println("Кол-во игр:");
        int k = scanner.nextInt();
        String[] arr = new String[n];
        int[] schet = new int[n];
        for (int i = 0; i < n; i++) {
            schet[i] = 0;
        }
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.next();
        }
        for (int i = 0; i < k; i++) {
            String a = scanner.next(); // Integer.ParceInt(c[1])
            String b = scanner.next();
            String c = scanner.next();
            String[] raz = c.split(":");
            int j = 0;
            do {
                if (a.equals(arr[j])) {
                    schet[j] = schet[j] + Integer.parseInt(raz[0]) - Integer.parseInt(raz[1]);
                }
                if (b.equals(arr[j])) {
                    schet[j] = schet[j] + Integer.parseInt(raz[1]) - Integer.parseInt(raz[0]);
                }
                j++;
            } while (j < n);
        }
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " " + schet[i]);
            System.out.println();
        }
    }
}
