package zad023_026;

/**
 * @author Damir Garifullin
 *         11-402
 *         023
 */


public class Task023 {
    public static void main(String[] args) {
        double sum = 0;
        double sum1;
        int n = 1;
        do {
            sum1 =  (double)(2 * n + 3) / (5 * n * n * n * n + 1);
            sum = sum + sum1 ;
            n = n + 1;
        } while (Math.abs(sum1) > 0.000000001);
        System.out.println(sum);
    }
}
