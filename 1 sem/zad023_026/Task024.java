package zad023_026;/**
 * @author Damir Garifullin
 * 11-402
 * 024
 */

import java.util.Scanner;

public class Task024 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextInt();
        double sum = 0;
        double sum1;
        double step = 1;
        int n = 1;
        do {
            step = step * 9 * (x - 1) * (x - 1);
            sum1 = 1.0 / (n * step);
            sum = sum + sum1;
            n = n + 1;
        } while (Math.abs(sum1) > 0.000000001);
        System.out.println(sum);
    }
}
