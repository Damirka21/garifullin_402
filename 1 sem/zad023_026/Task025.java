package zad023_026;/**
 * @author Damir Garifullin
 * 11-402
 * 025
 */

import java.util.Scanner;

public class Task025 {
    public static void main(String[] args) {
        double sum = 0;
        double sum1;
        double step = -1; // так как степень n+1
        int n = 1;
        do {
            step = step * -1;
            sum1 = (double)step / (n * n + 3 * n);
            sum = sum + sum1;
            n = n + 1;
        } while (Math.abs(sum1) > 0.000000001);
        System.out.println(sum);
    }
}
