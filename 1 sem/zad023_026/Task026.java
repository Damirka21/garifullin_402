package zad023_026;/**
 * @author Damir Garifullin
 * 11-402
 * 026
 */

import java.util.Scanner;

public class Task026 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextInt();
        double sum = 0;
        double sum1;
        double step = 1;
        int n = 1;
        do {
            step = step * (x-1) / (3 * n);
            sum1 = (double)step / (n * n + 3);
            sum = sum + sum1;
            n = n + 1;
        } while (Math.abs(sum1) > 0.000000001);
        System.out.println(sum);
    }
}
