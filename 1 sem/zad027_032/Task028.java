package zad027_032;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         028
 */
public class Task028 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int m = scanner.nextInt();
        int i ;
        for ( i = k ; i <= m ; i ++) {
            if ( i % 3 == 0) {
                System.out.println(i);
                break;
            }
        }
        for (int  j = i ; j <= m ; j += 3) {
            System.out.println(i);
        }
    }
}
