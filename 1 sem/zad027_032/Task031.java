package zad027_032;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         031
 */
public class Task031 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        int i;
        for (i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        boolean usl = true;
        i = 2; // чтоб на 1 шаге был по Факту 3 элемент
        while (usl && i < n) {
            if (a[i] % 3 == 0) {
                usl = true;
            } else {
                usl = false;
            }
            i = i + 3;
        }
        int sum = 0;
        int pro = 1;
        for (i = 0; i < n; i++) {
            if (a[i] > 0) {
                sum = sum + a[i];
                pro = pro * a[i];
            }
        }
        if (usl) {
            System.out.println(sum);
        } else {
            System.out.println(pro);
        }
    }
}
