package zad027_032;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         032
 */
public class Task032 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int i;
        int[] a1 = new int[m];
        int[] a2 = new int[n];
        int x;
        for (i = 0; i < m; i++) {
            a1[i] = scanner.nextInt();
        }
        for (i = 0; i < n; i++) {
            a2[i] = scanner.nextInt();
        }
        if (m > n) {
            x = m;
        } else {
            x = n;
        }
        int[] a3 = new int[x + 1];
        for (i = 0; i <= x; i++) {
            a3[i] = 0;
        }
        i = m - 1;
        int j = x;
        do {
            a3[j] = a1[i];
            i--;
            j--;
        } while (i >= 0);
        i = n - 1;
        j = x;
        do {
            a3[j] = a3[j] + a2[i];
            if (a3[j] > 9) {
                a3[j] = a3[j] % 10;
                a3[j - 1] = a3[j - 1] + 1;
            }
            i--;
            j--;
        } while (i >= 0);
        for (i = 0; i <= x; i++) {
            System.out.print(a3[i]);
        }
    }
}
