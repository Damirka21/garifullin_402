package zad027_032;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         029
 */
public class Task029 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int n = scanner.nextInt();
        int step = 1;
        int rez = 0;
        do {
            rez = rez + (n % 10) * step;
            step = step * k;
            n = n / 10;
        } while (n > 0);
        System.out.println(rez);
    }
}
