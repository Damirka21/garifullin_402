package zad027_032;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         030
 */
public class Task030 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = 1;
        int n = scanner.nextInt();
        int s3 = 0; //для кол-ва подходящих чисел
        do {
            int a = scanner.nextInt();
            int s1 = 0; //счетчик для кол-ва цифр
            int s2 = 0; //счетчик для кол-ва цифр
            boolean p1,p2;
            int b = a;
            do {
                p1 = a % 2 == 0;
                a = a / 10;
                s1 = s1 + 1;
            } while (a > 0 && p1);
            a = b;
            do {
                p2 = a % 2 == 1;
                a = a / 10;
                s2 = s2 + 1;
            } while (a > 0 && p2);
            if ((p1 | p2) && ((s1 == 3 | s1 == 5) |(s2 == 3 | s2 == 5))){
                s3 = s3 + 1;
            }
            i++;
        } while (i <= n && s3 <= 2);
        if (s3 == 2) {
            System.out.println("YES!");
        } else {
            System.out.println("NO!");
        }
        ;
    }
}