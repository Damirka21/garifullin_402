package zad027_032;

import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         027
 */

public class Task027 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean bool = false;
        int i = 1;
        do {

            if (i % 6 == 0) {
                bool = true;
            }

            i = i + 1;
        } while (!bool && i <= n);
        System.out.println(bool);
    }
}
