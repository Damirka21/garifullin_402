/**
 * @author Damir Garifullin
 * 11-402
 * 003
 */
public class Task003 {
    public static void main(String[] args) {
        int r = 2;
        double v = 4/ 3 * Math.PI * r * r * r;
        System.out.println(v);
    }
}
