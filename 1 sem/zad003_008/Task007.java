/**
 * @author Damir Garifullin
 * 11-402
 * 003
 * */
public class Task007 {
    public static void main(String[] args) {
        int a = 7;
        int b = 2;
        System.out.println("a + b = " + (a + b));
        System.out.println("a - b = " + (a - b));
        System.out.println("b - a = " + (b - a));
        System.out.println("a * b = " + (a * b));
        System.out.println("a / b = " + (a / b));
        System.out.println("a % b = " + (a % b));
        System.out.println("b / a = " + (b / a));
        System.out.println("b % a = " + (b % a));
    }
}
