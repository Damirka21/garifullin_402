package Task057;

import Task050.RationalFraction;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {
        RationalFraction rf = new RationalFraction(1, 2);
        RationalFraction rf1 = new RationalFraction(1, 3);
        RationalComplexNumber rcn = new RationalComplexNumber(rf1,rf);
        RationalComplexNumber rcn1 = new RationalComplexNumber(rf, rf1);
        RationalComplexNumber add = rcn.add(rcn1);
        RationalComplexNumber sub = rcn.sub(rcn1);
        RationalComplexNumber mult = rcn.mult(rcn);
        System.out.println(rcn.toString());
        System.out.println(rcn1.toString());
        System.out.println(add.toString());
        System.out.println(sub.toString());
        System.out.println(mult.toString());
    }
}
