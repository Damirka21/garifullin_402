package Task057;

import Task050.RationalFraction;
import Task051.ComplexNumber;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class RationalComplexNumber {
    public RationalFraction getR1() {
        return r1;
    }

    public void setR1(RationalFraction r1) {
        this.r1 = r1;
    }

    public RationalFraction getR2() {
        return r2;
    }

    public void setR2(RationalFraction r2) {
        this.r2 = r2;
    }

    private RationalFraction r1;
    private RationalFraction r2;

    public RationalComplexNumber(RationalFraction ra1, RationalFraction ra2) {
        r1 = ra1;
        r2 = ra2;
    }

    public RationalComplexNumber() {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalComplexNumber add(RationalComplexNumber rat) {
        RationalComplexNumber add = new RationalComplexNumber();
        add.setR1(this.r1.add(rat.getR1()));
        add.setR2(this.r2.add(rat.getR2()));
        return add;
    }

    public RationalComplexNumber sub(RationalComplexNumber rat) {
        RationalComplexNumber sub = new RationalComplexNumber();
        sub.setR1(this.r1.sub(rat.getR1()));
        sub.setR2(this.r2.sub(rat.getR2()));
        return sub;
    }

    public String toString() {
        return (r1.toString() + r2.toString() + "*i");
    }

    public RationalComplexNumber mult(RationalComplexNumber rat) {

        RationalFraction add = this.r1.mult(rat.r1);
        RationalFraction add1 = this.r2.mult(rat.r2);
        RationalFraction add3 = this.r1.mult(rat.r2);
        RationalFraction add2 = this.r2.mult(rat.r1);
        RationalFraction newAdd = add.sub(add1);
        RationalFraction newAdd2 = add2.add(add3);
        RationalComplexNumber mult = new RationalComplexNumber(newAdd, newAdd2);
        return mult;
    }
    public RationalComplexNumber div(RationalComplexNumber complexNumber) {
        RationalFraction a = complexNumber.r1.mult(complexNumber.r1).add(complexNumber.r2.mult(complexNumber.r2));
        RationalComplexNumber div = new RationalComplexNumber();
        div.r1 =  (this.r1.mult(complexNumber.r1).sub(this.r2.mult(complexNumber.r2))).div(a) ;
        div.r2 = (this.r1.mult(complexNumber.r2).add(this.r2 .mult(complexNumber.r1))).div(a);
        return div;

    }
}
