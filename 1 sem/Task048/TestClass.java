package Task048;


/**
 * @author Damir Garifullin
 *         11-402
 *         048
 */
public class TestClass {
    public static void main(String[] args) {
        Teacher teacher = new Teacher("Lebedev I.R.","History");
        Student student = new Student("Kozlov V.S.");
        teacher.assessStudent(student);
    }
}
