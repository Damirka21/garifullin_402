package Task048;

import java.util.Random;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Teacher {
    private String fio;
    private String predmet;

    public Teacher(String f, String p) {
        fio = f;
        predmet = p;
    }

    public String getFio() {
        return fio;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setFio(String f) {
        fio = f;
    }

    public void setPredmet(String p) {
        predmet = p;
    }

    public void assessStudent(Student student){
        Random random = new Random();
        int a = random.nextInt(4);
        a = a + 2;
        String b;
        switch (a) {
            case 2: b = "bad";
                break;
            case 3: b = "not bad";
                break;
            case 4: b = "good";
                break;
            case 5: b = "excellent";
                break;
            default: b = " ";
                break;
        }
        System.out.println("Teacher " + this.fio +
                " assess student " + student.getFio() +
                " by subject " + this.predmet +
                   " by mark  '" + b + "'");
    }
}
