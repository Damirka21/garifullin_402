package Task049;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {
        Vector2D v1 = new Vector2D();
        Vector2D v2 = new Vector2D(2, 5);
        Vector2D v3 = new Vector2D(1, 4);
        v2.mult(5);
        System.out.println("v1 = " + v1.toString());
        System.out.println("v2 x 5 = " + v2.toString());
        Vector2D v4 = v3.add(v3);
        v2.add2(v3);
        System.out.println("v2 add v3 =" + v2.toString());
        System.out.println("v4 = " + v4.toString());
        Vector2D v5 = v1.sub(v3);
        v1.sub2(v3);
        Vector2D v6 = v5;
        System.out.println("v1 sub v3 =" + v1.toString());
        System.out.println("v5 = " + v5.toString());
        System.out.println(v3.cos(v2));
        System.out.println(v3.scalarProduct(v2));
        System.out.println(v3.lenght());
        System.out.println(v2.lenght());
        System.out.println(v2.equals(v3));
        System.out.println(v5.equals(v6));
    }
}
