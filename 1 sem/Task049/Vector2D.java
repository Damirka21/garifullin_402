package Task049;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Vector2D {

    private double x, y;

    public String toString() {
        return "<" + x + "," + y + ">";
    }

    public Vector2D add(Vector2D vec1) {
        return new Vector2D(vec1.getX() + this.x, vec1.getY() + this.y);
    }

    public void add2(Vector2D vec1) {
        this.x += vec1.getX();
        this.y += vec1.getY();

    }

    public Vector2D sub(Vector2D vec1) {
        return new Vector2D(this.x - vec1.getX(), this.y - vec1.getY());
    }

    public void sub2(Vector2D vec1) {
        this.x -= vec1.getX();
        this.y -= vec1.getY();
        ;
    }

    public void mult(double n) {
        this.x = n * x;
        this.y = n * y;
    }

    public Vector2D mult2(double n) {
        return new Vector2D(x * n, y * n);
    }

    public double lenght() {
        double d = Math.sqrt(this.x * this.x + this.y * this.y);
        return d;
    }

    public double scalarProduct(Vector2D vector1) {
        double s = vector1.x * this.x + vector1.y * this.y;
        return s;
    }

    public double cos(Vector2D vector2D) {
        double cos = Math.abs(this.scalarProduct(vector2D)) / (this.lenght() * vector2D.lenght());
        return cos;
    }

    public boolean equals(Vector2D vector2D) {
        boolean c = this.x == vector2D.x && this.y == vector2D.y;
        return c;
    }

    public Vector2D() {
        this(0, 0);
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        x = x;
    }

    public void setY(double y) {
        y = y;
    }
}
