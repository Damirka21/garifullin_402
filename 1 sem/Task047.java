import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 *         047
 */
public class Task047 {
    public static String tabl(int k) {
        for (int i = 2; i < 10; i++) {
            System.out.println(i + "x" + k + "=" + (i * k));
        }
        return "";
    }

    public static double summa(double x) {
        double sum = 0;
        double slag = 1;
        int n = 1;
        do {
            slag = slag * 1.0 / (n * 9 * (x - 1) * (x - 1));
            sum = sum + slag;
            n++;
        } while (Math.abs(slag) > 0.000000001);
        return sum;
    }

    public static int proiz (int[] a, int[] b) {
        int pro = 0;
        for (int i = 0; i < a.length; i++) {
            pro = pro + a[i] * b[i];
        }
        return pro;
    }

    public static String ugol (int[]a ,int[] b){
        int ad = 0, bd =0, pro = 0;
        for (int i = 0; i < a.length; i++) {
            pro = pro + a[i] * b[i];
            ad = a[i] * a[i] + ad;
            bd = b[i] * b[i] + bd;
        }
        double u = Math.acos(Math.abs(pro) / (Math.sqrt(ad) * Math.sqrt(bd))) / Math.PI * 180;
        return (u + " degrees");
    }

    public static double[][] triangle(double[][] m, int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                double c =  m[j][i] / m[i][i];
                for (int k = 0; k < n; k++){
                    m[j][k] = m[j][k]  - m[i][k] * c ;
                }
            }
        }
        return m;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число для таблицы умножения");
        int k = scanner.nextInt();
        tabl(k);
        System.out.println("Введите число для вычисления предела");
        double x = scanner.nextDouble();
        double ab = summa(x);
        System.out.println(ab);
        System.out.println("Введите кол-ва координат векторов");
        int n = scanner.nextInt();
        System.out.println("Первый вектор :");
        int[] a = new int[n];
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        System.out.println("Второй вектор :");
        for (int i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }
        k = proiz(a , b);
        System.out.println(k);
        String bc = ugol(a , b);
        System.out.println(bc);
        System.out.println("Введите размер матрицы");
        n = scanner.nextInt();
        double[][] m = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[i][j] = scanner.nextInt();
            }
        }
        triangle(m, n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(m[i][j] + " ");
            }
            System.out.println();
        }
    }
}
