package Task056;

import Task051.ComplexNumber;
import Task054.ComplexVector2D;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {
        ComplexNumber complex1 = new ComplexNumber(2, 5);
        ComplexNumber complex3 = new ComplexNumber();
        ComplexNumber complex4 = new ComplexNumber(4, 3);
        ComplexNumber complex2 = new ComplexNumber(6, 8);
        ComplexVector2D cv = new ComplexVector2D(complex1,complex3);
        ComplexMatrix2x2 cm2 = new ComplexMatrix2x2(complex1);
        ComplexMatrix2x2 cm3 = new ComplexMatrix2x2(complex1,complex2,complex3,complex4);
        ComplexMatrix2x2 cm1 = new ComplexMatrix2x2();
        System.out.println(cm1.toString());
        System.out.println(cm2.toString());
        System.out.println(cm3.toString());
        System.out.println(cm3.add(cm2));
        System.out.println(cm3.mult(cm2));
        System.out.println(cm3.det());
        System.out.println();
        System.out.println(cm3.multVector(cv));
    }
}
