package Task056;


import Task051.ComplexNumber;
import Task054.ComplexVector2D;
import com.sun.org.apache.xpath.internal.SourceTree;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class ComplexMatrix2x2 {

    public String toString() {
        System.out.println(this.a[0][0].toString() + "\t" + this.a[0][1].toString());
        System.out.println(this.a[1][0].toString() + "\t" + this.a[1][1].toString());
        return "";
    }

    public ComplexNumber[][] getA() {
        return a;
    }

    public void setA(ComplexNumber[][] a) {
        this.a = a;
    }

    private ComplexNumber[][] a = new ComplexNumber[2][2];

    public ComplexMatrix2x2(ComplexNumber cn1) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] = cn1;
            }
        }
    }

    public ComplexMatrix2x2(ComplexNumber cn1, ComplexNumber cn2, ComplexNumber cn3, ComplexNumber cn4) {
        this.a[0][0] = cn1;
        this.a[0][1] = cn2;
        this.a[1][0] = cn3;
        this.a[1][1] = cn4;
    }

    public ComplexMatrix2x2() {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.a[i][j] = new ComplexNumber();
            }
        }
    }

    public ComplexMatrix2x2 add(ComplexMatrix2x2 cm) {
        ComplexMatrix2x2 mat = new ComplexMatrix2x2();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                mat.a[i][j] = this.a[i][j].add(cm.a[i][j]);
            }
        }
        return mat;
    }

    public ComplexMatrix2x2 mult(ComplexMatrix2x2 b) {
        ComplexMatrix2x2 mult = new ComplexMatrix2x2();
        mult.a[0][0] = (this.a[0][0].mult(b.a[0][0])).add(this.a[0][1].mult(b.a[1][0]));
        mult.a[1][0] = (this.a[1][0].mult(b.a[0][0])).add(this.a[1][1].mult(b.a[1][0]));
        mult.a[0][1] = (this.a[0][0].mult(b.a[0][1])).add(this.a[0][1].mult(b.a[1][1]));
        mult.a[1][1] = (this.a[1][0].mult(b.a[0][1])).add(this.a[1][1].mult(b.a[1][1]));
        return mult;
    }

    public ComplexNumber det() {
        ComplexNumber c = this.a[1][0].div(this.a[0][0]);
        for (int k = 0; k < 2; k++) {
            this.a[1][k] = a[1][k].sub(this.a[0][k].mult(c));
        }
        c = this.a[1][1].mult(this.a[0][0]);
        return c;
    }

    public ComplexVector2D multVector(ComplexVector2D b) {
        ComplexNumber c = this.a[0][0].mult(b.getCom1()).add(this.a[0][1].mult(b.getCom2()));
        ComplexNumber d = this.a[1][0].mult(b.getCom1()).add(this.a[1][1].mult(b.getCom2()));
        return new ComplexVector2D(c, d);
    }
}
