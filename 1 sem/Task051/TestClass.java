package Task051;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class TestClass {
    public static void main(String[] args) {
        ComplexNumber complex1 = new ComplexNumber(2, 5);
        ComplexNumber complex0 = new ComplexNumber();
        ComplexNumber complex2 = new ComplexNumber(4, 3);
        ComplexNumber complex3 = complex1.add(complex2);
        System.out.println(complex3.toString());
        System.out.println(complex2.arg());
        System.out.println(complex2.lenght());
        System.out.println(complex2.pow(2.3));
        System.out.println(complex2.equals(complex0));
        System.out.println(complex2.equals(complex2));
        complex1.add2(complex2);
        complex2.sub2(complex1);
        complex3.mult2(complex1);
        System.out.println(complex1.toString());
        System.out.println(complex2.toString());
        System.out.println(complex3.toString());
        complex3.multNumber2(1.5);
        System.out.println(complex3.toString());
        ComplexNumber add = complex1.add(complex2);
        ComplexNumber sub = complex2.sub(complex1);
        ComplexNumber mult = complex3.mult(complex1);
        ComplexNumber multNumber = complex1.multNumber(2.4);
        System.out.println(add.toString());
        System.out.println(sub.toString());
        System.out.println(mult.toString());
        System.out.println(multNumber.toString());
    }
}
