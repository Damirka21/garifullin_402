package Homework;

import java.util.regex.*;

/**
 * Created by @Damir
 * on  20.09.2015
 * *
 */
public class Task02c {
    public static void main(String[] args) {
        String[] strings ={"09","010101", "11", "00","101","0110","001","11101"};
        Pattern pattern = Pattern.compile("((1*)|(0*)|((01)*0?)|((10)*1?))");
        int p =0;
        for (String i : strings) {
            Matcher matcher = pattern.matcher(i);
            if (matcher.matches()) {
                System.out.println(p);
            }
            p++;
        }

    }
}
