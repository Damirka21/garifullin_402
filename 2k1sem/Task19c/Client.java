import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by ma on 29.10.2015.
 */
public class Client {
    public static void main(String[] args) throws IOException {
        final int PORT = 3456;
        final String HOST = "localhost";
        Socket s = new Socket(HOST, PORT);

        PrintWriter os = new PrintWriter(
                s.getOutputStream(), true);
        BufferedReader is = new BufferedReader(
                new InputStreamReader(
                        s.getInputStream()
                )
        );
        Scanner scanner = new Scanner(System.in);
        System.out.println("What is your name? ");
        String clientName = scanner.nextLine();
        os.println(clientName);
        String serverName = "";
        while (serverName.equals("")) {
            serverName = is.readLine();
        }
        int i = 0;
        while (true) {
            String x = is.readLine();
            System.out.println(serverName + ": " + x);
            System.out.print(clientName + ": ");
            x = scanner.nextLine();
            os.println(x);
        }
    }
}
