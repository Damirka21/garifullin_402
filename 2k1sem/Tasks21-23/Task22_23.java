import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/**
 * Created by Damir on 17.11.2015.
 */
public class Task22_23 extends JFrame {

    private int size;
    private int minesNumber;
    private int buttonSize = 50;
    private int[][] field;
    Queue queue = new LinkedList<>();
    JButton[][] gameField;
    boolean[][] checked;

    public Task22_23(int s, int i1) {
        size = s;
        minesNumber = i1;
        generateField();
        queue = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(field[i][j]);
            }
            System.out.println();
        }
        checked = new boolean[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                checked[i][j] = false;
            }
        }
        gameField = new JButton[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                gameField[i][j] = new JButton("");
            }
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                gameField[i][j].setBounds(i * buttonSize, j * buttonSize, buttonSize, buttonSize);

                gameField[i][j].addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                            if (e.getButton() == MouseEvent.BUTTON3) {
                                JButton jb1 = (JButton) e.getSource();
                                if (jb1.isEnabled()) {
                                    if (jb1.getText().equals("P")) {
                                        jb1.setText("");
                                    } else {
                                        jb1.setText("P");
                                    }
                                }
                            } else if (e.getButton() == MouseEvent.BUTTON1) {
                                JButton jb = (JButton) e.getSource();
                                int x = jb.getX() / buttonSize;
                                int y = jb.getY() / buttonSize;
                                int count = countMinesAround(y, x);
                                if (field[y][x] == 1) {
                                    jb.setText("X");
                                    openBombs();
                                } else {
                                    jb.setText(count + "");
                                }
                                if (count == 0) {
                                    queue.add(jb);
                                    opening();
                                }
                                checked[x][y] = true;
                                jb.setEnabled(false);
                            }
                        if(isPlaying()) {
                            end(1);
                        }
                    }


                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
                add(gameField[i][j]);
            }
        }

        this.

                setLayout(new GroupLayout(getContentPane()

                ));

        setBounds(50, 50, buttonSize * size + 50, buttonSize * size + 50);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setVisible(true);

    }

    private boolean isPlaying() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (gameField[i][j].getText().equals("")) {
                    return false;
                }
            }
        }
        return true;
    }

    private void end(int i) {
        JFrame myFrame = new JFrame("showOptionDialog() Test");
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container myPane = myFrame.getContentPane();
        int messageType = JOptionPane.QUESTION_MESSAGE;
        String message= "";
        if (i == 0) {
            message = "Game over";
        } else {
            message = "Congratulations!";
        }
        String[] options = {"New Game", "Exit"};
        int code = JOptionPane.showOptionDialog(myFrame,
                message,
                "Option Dialog Box", 0, messageType,
                null, options, "PHP");

        if (code == 0) {

            for (int i1 = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    field[i1][j] = 0;
                }
            }
            setVisible(false);
            new Task22_23(10, 10);
        } else {
            System.exit(1);
        }
    }

    private void openBombs() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (field[j][i] == 1) {
                    gameField[i][j].setText("X");
                }
            }
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                field[j][i] = 0;
            }
        }

        end(0);


    }

    private void opening() {
        while (!queue.isEmpty()) {
            JButton jb = (JButton) queue.poll();
            int x = jb.getX() / buttonSize;
            int y = jb.getY() / buttonSize;
            for (int i = -1; i <= 1; i++) {
                if (x + i >= 0 && x + i < size) {
                    for (int j = -1; j <= 1; j++) {
                        if (y + j >= 0 && y + j < size) {
                            if (i != 0 || j != 0) {
                                if (!checked[x + i][y + j]) {
                                    int count = countMinesAround(y + j, x + i);
                                    gameField[x + i][y + j].setText(count + "");
                                    gameField[x + i][y + j].setEnabled(false);
                                    if (count == 0) {
                                        queue.add(gameField[x + i][y + j]);
                                    }
                                    checked[x + i][y + j] = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private int countMinesAround(int x, int y) {
        int s = 0;
        for (int i = -1; i <= 1; i++) {
            if (x + i >= 0 && x + i < size) {
                for (int j = -1; j <= 1; j++) {
                    if (y + j >= 0 && y + j < size) {
                        s += field[x + i][y + j];
                    }
                }
            }
        }
        return s;
    }

    private void generateField() {
        field = new int[size][size];
        Random r = new Random();
        int k = 0;
        while (k < minesNumber) {
            int i = r.nextInt(size);
            int j = r.nextInt(size);
            if (field[i][j] == 0) {
                field[i][j] = 1;
                k++;
            }
        }
    }

    public static void main(String[] args) {
        new Task22_23(10, 10);
    }
}
