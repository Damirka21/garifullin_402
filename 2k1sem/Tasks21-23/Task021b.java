/**
 * Created by Damir on 20.11.2015.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ruslanzigansin on 18.11.15.
 */
public class Task021b extends JFrame {
    JTextArea jt = new JTextArea();
    JPanel jbPanel = new JPanel(new GridLayout(4, 4));
    JPanel jbPanel1 = new JPanel(new GridLayout(1, 3));
    JButton jb0 = new JButton("0");
    JButton jb1 = new JButton("1");
    JButton jb2 = new JButton("2");
    JButton jb3 = new JButton("3");
    JButton jb4 = new JButton("4");
    JButton jb5 = new JButton("5");
    JButton jb6 = new JButton("6");
    JButton jb7 = new JButton("7");
    JButton jb8 = new JButton("8");
    JButton jb9 = new JButton("9");
    JButton jbSum = new JButton("+");
    JButton jbC = new JButton("C");
    JButton jbDiv = new JButton("/");
    JButton jbSub = new JButton("-");
    JButton jbMult = new JButton("*");
    JButton jbResult = new JButton("=");
    JButton jbPoint = new JButton(".");
    JButton jbAC = new JButton("AC");
    JButton jbPlusMinus = new JButton("+/-");
    double val1;
    String operation;
    boolean check = true;
    boolean symbol = true;

    public void setFalse() {
        check = false;
    }

    public String checkNumber(int i) {
        if (check == true) {
            return i + "";
        } else {
            return (jt.getText() + i);
        }
    }

    public void setOperation(String s) {
        val1 = Double.valueOf(jt.getText());
        jt.setText("");
        symbol = true;
        operation = s;
    }

    public Task021b() {
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(300, 300, 300, 300);
        setLayout(new BorderLayout());
        add(jt, BorderLayout.NORTH);
        add(jbPanel, BorderLayout.CENTER);
        add(jbPanel1, BorderLayout.SOUTH);
        jbPanel1.add(jbAC);
        jbPanel1.add(jbC);
        jbPanel1.add(jbPlusMinus);
        jbPanel.add(jb7);
        jbPanel.add(jb8);
        jbPanel.add(jb9);
        jbPanel.add(jbDiv);
        jbPanel.add(jb4);
        jbPanel.add(jb5);
        jbPanel.add(jb6);
        jbPanel.add(jbMult);
        jbPanel.add(jb1);
        jbPanel.add(jb2);
        jbPanel.add(jb3);
        jbPanel.add(jbSub);
        jbPanel.add(jb0);
        jbPanel.add(jbPoint);
        jbPanel.add(jbResult);
        jbPanel.add(jbSum);
        setVisible(true);


        jbPlusMinus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!check) {
                    if (symbol) {
                        jt.setText("-" + jt.getText());
                        symbol = false;
                    } else {
                        String s = "";
                        for (int i = 1; i < jt.getText().length(); i++) {
                            s += jt.getText().charAt(i);
                        }
                        jt.setText(s);
                        symbol = true;
                    }
                }else{
                    jt.setText("");
                }
            }
        });
        jb0.addActionListener(new ActionListener() {
                                  @Override
                                  public void actionPerformed(ActionEvent e) {
                                      jt.setText(checkNumber(0));
                                      setFalse();
                                  }
                              }

        );
        jb1.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(1));

                                              setFalse();
                                          }
                                      }

        );
        jb2.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(2));
                                              setFalse();
                                          }
                                      }

        );
        jb3.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(3));

                                              setFalse();
                                          }
                                      }

        );
        jb4.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(4));
                                              setFalse();
                                          }
                                      }

        );
        jb5.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(5));
                                              setFalse();
                                          }
                                      }

        );
        jb6.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(6));
                                              setFalse();
                                          }
                                      }

        );
        jb7.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(7));
                                              setFalse();
                                          }
                                      }

        );
        jb8.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(8));
                                              setFalse();
                                          }
                                      }

        );
        jb9.addActionListener(new

                                      ActionListener() {
                                          @Override
                                          public void actionPerformed(ActionEvent e) {
                                              jt.setText(checkNumber(9));
                                              setFalse();
                                          }
                                      }

        );
        jbC.addActionListener(new

                                         ActionListener() {
                                             @Override
                                             public void actionPerformed(ActionEvent e) {
                                                 jt.setText("");
                                                 setFalse();

                                             }
                                         }

        );

        jbAC.addActionListener(new

                                          ActionListener() {
                                              @Override
                                              public void actionPerformed(ActionEvent e) {
                                                  jt.setText("");
                                                  setFalse();
                                              }
                                          }

        );
        jbSum.addActionListener(new

                                        ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                    setOperation("+");
                                                    setFalse();
                                            }
                                        }

        );
        jbSub.addActionListener(new

                                        ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                    setOperation("-");
                                                    setFalse();

                                            }
                                        }

        );
        jbMult.addActionListener(new

                                         ActionListener() {
                                             @Override
                                             public void actionPerformed(ActionEvent e) {
                                                 setOperation("*");
                                                 setFalse();
                                             }
                                         }

        );
        jbDiv.addActionListener(new

                                        ActionListener() {
                                            @Override
                                            public void actionPerformed(ActionEvent e) {
                                                setOperation("/");
                                                setFalse();
                                            }
                                        }

        );
        jbPoint.addActionListener(new

                                          ActionListener() {
                                              @Override
                                              public void actionPerformed(ActionEvent e) {
                                                  jt.setText(jt.getText() + ".");
                                                  setFalse();

                                              }
                                          }

        );
        jbResult.addActionListener(new

                                           ActionListener() {
                                               @Override
                                               public void actionPerformed(ActionEvent e) {
                                                   double val2 = Double.valueOf(jt.getText());
                                                   switch (operation) {
                                                       case "+":
                                                           jt.setText(val1 + val2 + "");
                                                           break;
                                                       case "-":
                                                           jt.setText(val1 - val2 + "");
                                                           break;
                                                       case "*":
                                                           jt.setText(val1 * val2 + "");
                                                           break;
                                                       case "/":
                                                           jt.setText(val1 / val2 + "");
                                                           break;
                                                   }
                                                   check = true;

                                               }
                                           }

        );
    }


    public static void main(String[] args) {
        new Task021b();
    }
}
