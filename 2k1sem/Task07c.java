package Homework;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by @Damir
 * on  21.09.2015
 * *
 */
public class Task07c {
    public static void main(String[] args) throws IOException {
        Pattern pattern = Pattern.compile("http://.*?(([a-zA-Z_0-9-]*)=([a-zA-Z_0-9-][a-zA-Z_0-9-]*)((\\&([a-zA-Z_0-9-]*)=([a-zA-Z_0-9-][a-zA-Z_0-9-]*))?)((\\&([a-zA-Z_0-9-]*)=([a-zA-Z_0-9-][a-zA-Z_0-9-]*))?))");
        File file = new File("example.html");
        BufferedReader reader  = new BufferedReader(new FileReader(file));
        String line = "";
        while ((line = reader.readLine()) != null){
            Matcher matcher = pattern.matcher(line);

            while(matcher.find()){
                System.out.println("PARAMSTRING : " + matcher.group(1) );
                System.out.println("NAME : " + matcher.group(2));
                System.out.println("VALUE : " + matcher.group(3));
                if(matcher.group(6)!=null){
                    System.out.println("NAME : " + matcher.group(6));
                    System.out.println("VALUE : " + matcher.group(7));
                    if(!matcher.group(10).equals(null)){
                        System.out.println("NAME : " + matcher.group(10));
                        System.out.println("VALUE : " + matcher.group(11));
                    }
                }

            }


        }
        reader.close();
    }
}
