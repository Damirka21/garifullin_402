import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Damir on 12.10.2015.
 */
@WebServlet(name = "search")
public class search extends VelocityServlet {
    public Template handleRequest( HttpServletRequest request,
                                   HttpServletResponse response,
                                   Context context ) {

        Template template = null;
        if (request.getMethod().equals("GET")) {
            String path1 = request.getPathInfo();
            String path = "";
            for (int i = 1; i < path1.length(); i++) {
                path = path + path1.charAt(i);
            }
            if (path.equals("baidu.com")) {
                context.put("result", 1);
            }
            if (path.equals("bing.com")) {
                context.put("result", 2);
            }
            if (path.equals("yahoo.com")) {
                context.put("result", 3);
            }
            if (path.equals("aol.com")) {
                context.put("result", 4);
            }
            try {
                template = Velocity.getTemplate("search.vm");
            } catch (Exception e) {
                System.err.println("Exception caught: " + e.getMessage());
            }
        }
        return template;
    }
}
