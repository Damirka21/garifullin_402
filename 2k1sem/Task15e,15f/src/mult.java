import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Damir on 12.10.2015.
 */
@WebServlet(name = "mult")
public class mult extends VelocityServlet {
    public Template handleRequest( HttpServletRequest request,
                                                                          HttpServletResponse response,
                                                                          Context context ) {

    Template template = null;
        if (request.getMethod().equals("GET")) {
            String path = request.getPathInfo();
            String[] arr = path.split("/");
            int a = Integer.parseInt(arr[1]);
            int b = Integer.parseInt(arr[2]);
            try {
                context.put("result", a * b);
                template = Velocity.getTemplate("result.vm");
            } catch (Exception e) {
                System.err.println("Exception caught: " + e.getMessage());
            }
        }
    return template;
}
}
