import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Damir on 12.10.2015.
 */
@WebServlet(name = "result")
public class result extends VelocityServlet {
    public Template handleRequest( HttpServletRequest request,
                                   HttpServletResponse response,
                                   Context context ) {

        Template template = null;
        HttpSession session = request.getSession();
        if (request.getMethod().equals("GET")) {
            try {
                context.put("result",session.getAttribute("result"));
                template = Velocity.getTemplate("result.vm");
            } catch (Exception e) {
                System.err.println("Exception caught: " + e.getMessage());
            }
        }
        return template;
    }
}
