import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.context.Context;
import org.apache.velocity.servlet.VelocityServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Damir on 12.10.2015.
 */
@WebServlet(name = "process")
public class process extends VelocityServlet {
    public Template handleRequest( HttpServletRequest request,
                                                                         HttpServletResponse response,
                                                                         Context context ) throws IOException {

    Template template = null;

    if (request.getMethod().equals("GET")) {
        try {
            context.put("form_url", request.getRequestURI());
            template = Velocity.getTemplate("process.vm");
        } catch (Exception e) {
            System.err.println("Exception caught: " + e.getMessage());
        }

    } else if (request.getMethod().equals("POST")){
        String text = request.getParameter("text");
        HttpSession session = request.getSession();
        int result = 0;
        switch (request.getParameter("process")) {
            case ("symbols"):
                for (int i = 0; i < text.length(); i++) {
                    if(text.charAt(i)!=' '){
                        result++;
                    }
                }
                break;
            case ("words"):
                String[] words = text.split(" ");
                result = words.length;
                break;
            case ("sentences"):
                String[] sentences = text.split("[!\\?\\.]");
                result = sentences.length;
                break;
            case ("paragraphs"):
                String[] parag = text.split("\\n");
                result = parag.length;
                break;
        }
        context.put("result", result);
        session.setAttribute("result",result);
        response.sendRedirect("/result");
    }
        return template;
}
}
