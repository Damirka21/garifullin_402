import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Damir on 02.10.2015.
 */
@WebServlet(name = "mult")
public class mult extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        String path = request.getPathInfo();
        String[] arr = path.split("/");
        int a = Integer.parseInt(arr[1]);
        int b = Integer.parseInt(arr[2]);
        pw.println(a * b);
        pw.close();
    }
}
