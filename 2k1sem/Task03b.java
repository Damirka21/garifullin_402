package Homework;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by @Damir
 * on  20.09.2015
 * *
 */


public class Task03b {
    public static void main(String[] args) {
        Random random = new Random();
        int count = 0, p1 = 0, p2 = 0;
        Pattern pattern1 = Pattern.compile("[0-9]*[02468]{3}[0-9]*");
        Pattern pattern2 = Pattern.compile("^[0-9]*[02468]{3}[0-9]*$");
        while (p1 < 10 & p2< 10) {
            int a = random.nextInt(Integer.MAX_VALUE);
            String check = String.valueOf(a);
            Matcher matcher1 = pattern1.matcher(check);
            Matcher matcher2 = pattern2.matcher(check);
            if (a > 0 && !matcher1.matches()) {
                p1++;
                System.out.println(a);
            }
            if (a > 0 && !matcher2.find()) {
                p2++;
                System.out.println(a);
            }
            count ++;
        }
        System.out.println(count);
    }
}
