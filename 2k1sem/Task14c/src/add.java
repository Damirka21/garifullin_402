import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by Damir on 02.10.2015.
*/
@WebServlet(name = "add")
public class add extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("result.ftl");
        HashMap<String, Object> root = new HashMap<>();
        root.put("form_url",request.getRequestURI());
        String path = request.getPathInfo();
        String[] arr = path.split("/");
        int a = Integer.parseInt(arr[1]);
        int b = Integer.parseInt(arr[2]);
        root.put("result",a+b);
        try {
            tmpl.process(root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
