import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by Damir on 02.10.2015.
 */
@WebServlet(name = "search")
public class search extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("search.ftl");
        HashMap<String, Object> root = new HashMap<>();
        root.put("form_url",request.getRequestURI());


        String path1 = request.getPathInfo();
        String path = "" ;
        for (int i = 1; i < path1.length(); i ++){
            path = path + path1.charAt(i);
        }
        if(path.equals("baidu.com")){root.put("result",1);}
        if(path.equals("bing.com")){root.put("result",2);}
        if(path.equals("yahoo.com")){root.put("result",3);}
        if(path.equals("aol.com")){root.put("result",4);}
        try {
            tmpl.process(root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}