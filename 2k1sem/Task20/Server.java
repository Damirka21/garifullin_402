package nov10;

import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Damir on 10.11.2015.
 */
public class Server implements Serializable{
    final int PORT = 3456;
    ArrayList<Connection> connections;
    public static ArrayList<Message> messages = new ArrayList<>();

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void addMessage(Message message) {this.messages.add(message);}

    public Server() throws IOException {
        connections = new ArrayList<>();
        go();
    }

    public void go() throws IOException {
        ServerSocket s1 = new ServerSocket(PORT);
        while (true) {
            Socket client = s1.accept();
            connections.add(new Connection(this, client));
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server();

    }
}