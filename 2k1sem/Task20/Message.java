package nov10;

import java.util.Calendar;

/**
 * Created by Damir on 10.11.2015.
 */
public class Message {
    public String text;
    public String Client;

    public Message(String text, String client, Calendar dateTime) {
        this.text = text;
        this.Client = client;
        this.dateTime = dateTime;
    }

    public Calendar dateTime;

    @Override
    public String toString() {
        return this.Client + "  " + this.dateTime.get(Calendar.HOUR_OF_DAY) + ":" + this.dateTime.get(Calendar.MINUTE) +
                ":" + this.dateTime.get(Calendar.SECOND) + " :   " + this.text;
    }
}
