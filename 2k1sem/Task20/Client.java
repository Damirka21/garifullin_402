package nov10;

import java.io.*;
import java.net.Socket;
import java.sql.Time;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by ma on 29.10.2015.
 */
public class Client {
    public static void main(String[] args) throws IOException {
        final int PORT = 3456;
        final String HOST = "localhost";
        Socket s = new Socket(HOST, PORT);
        BufferedReader is = new BufferedReader(
                new InputStreamReader(
                        s.getInputStream()
                )
        );
        PrintWriter os = new PrintWriter(
                s.getOutputStream(), true);
        Scanner scanner = new Scanner(System.in);
        System.out.println("What is your name? ");
        String clientName = scanner.nextLine();
        os.println(clientName);
        while (true) {
            System.out.print(clientName + ": ");
            String x = scanner.nextLine();
            os.println(x);
            while(is.ready()){
                System.out.println(is.readLine());
            }

        }
    }
}
