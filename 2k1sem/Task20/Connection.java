package nov10;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

/**
 * Created by Damir on 10.11.2015.
 */
class Connection implements Runnable,Serializable {
    Socket socket;
    Thread thread;
    Server server;
    public Connection(Server server, Socket socket) {
        this.socket = socket;
        this.server = server;
        thread = new Thread(this);
        thread.start();
    }
    public void run() {
        try {
            PrintWriter os = new PrintWriter(
                    socket.getOutputStream(), true);
            BufferedReader is = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()
                    )
            );
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            String clientName = is.readLine();
            System.out.println(clientName);
            while(true){
                String message = is.readLine();
                System.out.println(message);
                Calendar calendar = Calendar.getInstance();
                Message s = new Message(message,clientName,calendar);
                server.addMessage(s);
                for (Message s1 : server.getMessages()) {
                    os.println(s1);
                }
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
