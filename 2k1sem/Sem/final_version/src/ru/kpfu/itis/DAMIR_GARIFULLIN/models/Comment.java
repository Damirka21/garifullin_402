package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Damir on 09.11.2015.
 */
public class Comment {
    int articleId;
    String author;
    String content;

    public Comment(int articleId, String author, String content, ArrayList<String> files, Date dateTime, int id) {
        this.articleId = articleId;
        this.author = author;
        this.content = content;
        this.files = files;
        this.dateTime = dateTime;
        this.id = id;
    }

    public int getArticleId() {
        return articleId;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public ArrayList<String> getFiles() {
        return files;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public int getId() {
        return id;
    }

    ArrayList<String> files;
    Date dateTime;
    int id;
}
