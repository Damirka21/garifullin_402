package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

/**
 * Created by Damir on 09.11.2015.
 */
public class Online {
    public Online(String name, String description, String url) {
        this.name = name;
        this.description = description;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    String name;
    String description;
    String url;
}
