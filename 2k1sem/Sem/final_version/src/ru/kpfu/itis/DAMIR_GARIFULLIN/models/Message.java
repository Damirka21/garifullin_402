package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

import java.util.ArrayList;

/**
 * Created by Damir on 09.11.2015.
 */
public class Message {
    public Message(String recipient, String sender, String content, ArrayList<String> files) {
        this.recipient = recipient;
        this.sender = sender;
        this.content = content;
        this.files = files;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getSender() {
        return sender;
    }

    public String getContent() {
        return content;
    }

    public ArrayList<String> getFiles() {
        return files;
    }

    String recipient;
    String sender;
    String content;
    ArrayList<String> files;
}
