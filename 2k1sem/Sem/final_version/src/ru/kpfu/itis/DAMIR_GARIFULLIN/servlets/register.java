package ru.kpfu.itis.DAMIR_GARIFULLIN.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.ConfigSingletone;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Damir on 09.11.2015.
 */
@WebServlet(name = "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.register")
public class register extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap hashMap = new HashMap();
        try {
             hashMap = UserRepository.getUsers();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        HttpSession session = request.getSession();
        String user = (String) request.getParameter("username");
        String password1 = (String) request.getParameter("password1");
        String password2 = (String) request.getParameter("password2");
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        if (hashMap.containsKey(user)){
            login.root.put("message","User is alredy exists");
            response.sendRedirect("/register");
        }else{
            if(!password1.equals(password2)){
                login.root.put("message","Password mismatch");
                response.sendRedirect("/register");
            }else{
                try {
                    UserRepository.addUser(user,password1, Date.valueOf(request.getParameter("birthday")),request.getParameter("email"));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                response.sendRedirect("/profile");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        login.root.put("request",request.getRequestURI());
        Template tmpl = cfg.getTemplate("register.ftl");

        try {
            tmpl.process(login.root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
        if (login.root.containsKey("message")){
            login.root.remove("message");
        }
    }
}
