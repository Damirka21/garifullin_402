package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Article;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Voting;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Damir on 09.11.2015.
 */
public class VoitingRepository {

    public static ArrayList getVotings() throws SQLException {
        ArrayList arrayList = new ArrayList();
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("SELECT *  FROM votings ");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Voting voting = new Voting(rs.getString("question"),rs.getInt("id"),rs.getString("answer1"),
                        rs.getString("answer2"),rs.getString("answer3"),
                        rs.getString("answer4"),rs.getInt("a1"),rs.getInt("a2"),rs.getInt("a3"),rs.getInt("a4"));
                arrayList.add(voting);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return arrayList;
    }

}
