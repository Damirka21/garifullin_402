package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

import java.util.ArrayList;

/**
 * Created by Damir on 09.11.2015.
 */
public class Article {
    public int getId() {
        return id;
    }

    int id;
    public String name;

    public Article(int id,String name, String author, String content, String files) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.content = content;
        this.files = files;
    }

    public String author;
    public String content;

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getContent() {
        return content;
    }

    public String getFiles() {
        return files;
    }

    public String files;
}
