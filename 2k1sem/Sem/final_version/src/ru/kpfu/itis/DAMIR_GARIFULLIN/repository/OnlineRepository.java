package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Article;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Online;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Damir on 09.11.2015.
 */
public class OnlineRepository {
    public static ArrayList<Online> getOnline() throws SQLException {
        ArrayList<Online> arrayList = new ArrayList<Online>();
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("SELECT *  FROM online ");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Online online = new Online(rs.getString("name"),rs.getString("description"),rs.getString("url"));
                arrayList.add(online);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return arrayList;
    }
}
