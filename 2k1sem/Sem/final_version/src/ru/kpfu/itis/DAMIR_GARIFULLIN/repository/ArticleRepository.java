package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Article;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.User;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Damir on 09.11.2015.
 */
public class ArticleRepository {
    public static ArrayList getArticles() throws SQLException {
        ArrayList arrayList = new ArrayList();
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("SELECT *  FROM articles ");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                ArrayList<String> files = new ArrayList<>();
                rs.getString("files");
                String[] fil = rs.getString("files").split(",");
                for (String s : fil){
                    files.add(s);
                }
                Article article = new Article(rs.getInt("id"),rs.getString("name"),rs.getString("author"),rs.getString("content"),rs.getString("files"));
                arrayList.add(article);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return arrayList;
    }
    public static void  addArticle(String name, String author, String content, ArrayList<String> files) throws SQLException {
        try {
            Connection conn = MyConnection.getConnection();
            String filesS = "";
            for (String s : files){
                filesS += s;
            }
            PreparedStatement ps = conn.prepareStatement("INSERT INTO articles (name,author,content,files) VALUES (?,?,?,?)");
            ps.setString(1, name);
            ps.setString(2, author);
            ps.setString(3, content);
            ps.setString(4, filesS);
            ps.execute();
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
    }
}
