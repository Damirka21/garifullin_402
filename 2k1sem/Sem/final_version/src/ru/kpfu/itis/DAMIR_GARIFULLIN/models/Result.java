package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

/**
 * Created by Damir on 15.11.2015.
 */
public class Result {
    String team1;
    String team2;
    int t1;
    int t2;

    public String getTeam1() {
        return team1;
    }

    public String getTeam2() {
        return team2;
    }

    public int getT1() {
        return t1;
    }

    public int getT2() {
        return t2;
    }

    public Result(String team1, String team2, int t1, int t2) {
        this.team1 = team1;
        this.team2 = team2;
        this.t1 = t1;
        this.t2 = t2;
    }
}
