package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Article;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Damir on 09.11.2015.
 */
public class MessageRepository {
    public static ArrayList getMessages() throws SQLException {
        ArrayList arrayList = new ArrayList();
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("SELECT *  FROM messages ");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                ArrayList<String> files = null;
                String[] fil = null;
                if (rs.getString("files") != null) {
                    rs.getString("files");
                    fil = rs.getString("files").split(",");
                    files = new ArrayList<>();
                    for (String s : fil) {
                        files.add(s);
                    }
                }

                Message message = new Message( rs.getString("recipient"),rs.getString("sender"), rs.getString("content"), files);
                arrayList.add(message);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return arrayList;
    }

    public static void addMessage(String recipient, String sender, String content, ArrayList<String> files) throws SQLException {
        try {
            Connection conn = MyConnection.getConnection();
            String filesS = "";
            for (String s : files) {
                filesS += s;
            }
            PreparedStatement ps = conn.prepareStatement("INSERT INTO messages (recipient,sender,content,files) VALUES (?,?,?,?)");
            ps.setString(1, recipient);
            ps.setString(2, sender);
            ps.setString(3, content);
            ps.setString(4, filesS);
            ps.execute();
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
    }
}
