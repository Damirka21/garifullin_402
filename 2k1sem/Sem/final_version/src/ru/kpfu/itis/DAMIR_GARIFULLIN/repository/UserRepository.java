package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import ru.kpfu.itis.DAMIR_GARIFULLIN.models.User;

import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.security.MessageDigest;

/**
 * Created by Damir on 09.11.2015.
 */
public class UserRepository {
    public static HashMap getUsers() throws SQLException {
        HashMap hashMap = new HashMap();
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("SELECT *  FROM users ");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("id"),rs.getString("login"),rs.getString("password"),rs.getDate("birthday"),rs.getString("email"));
                hashMap.put(user.getLogin(),user);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return hashMap;
    }
    public static void  addUser(String login, String password, Date birthday,String email) throws SQLException {
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("INSERT INTO users(login,password,birthday,email) VALUES (?,?,?,?)");
            String a= password; // тут исходный пароль
            MessageDigest md=MessageDigest.getInstance("SHA-1");
            md.update(a.getBytes());
            String b = new String(md.digest());
            ps.setString(1, login);
            ps.setString(2, b);
            ps.setDate(3, birthday);
            ps.setString(4, email);
            ps.execute();
            conn.close();
        } catch (SQLException | ClassNotFoundException | NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
    }
}
