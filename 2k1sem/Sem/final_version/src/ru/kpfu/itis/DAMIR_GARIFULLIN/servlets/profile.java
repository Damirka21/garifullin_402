package ru.kpfu.itis.DAMIR_GARIFULLIN.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.User;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.ConfigSingletone;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Damir on 29.09.2015.
 */
@WebServlet(name = "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.profile")
public class profile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String user = (String)session.getAttribute("current_user");

        Configuration cfg = ConfigSingletone.getConfig(getServletContext());

        login.root.put("request",request.getRequestURI());
        Template tmpl = cfg.getTemplate("profile.ftl");
        if(user == null){
            response.sendRedirect("/login");
        }
        try {
            HashMap<String,User> hashMap = UserRepository.getUsers();
            login.root.put("user",hashMap.get(user));
            tmpl.process(login.root,response.getWriter());
        } catch (TemplateException | SQLException e) {
            e.printStackTrace();
        }
    }
}
