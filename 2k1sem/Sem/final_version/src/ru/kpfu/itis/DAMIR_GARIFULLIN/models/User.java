package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

import java.util.Date;

/**
 * Created by Damir on 09.11.2015.
 */
public class User {
    int id ;
    String login;

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    String password;
    Date birthday;

    public int getId() {
        return id;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getEmail() {
        return email;
    }

    public User(int id, String login, String password, Date birthday, String email) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.birthday = birthday;
        this.email = email;

    }

    String email;
}
