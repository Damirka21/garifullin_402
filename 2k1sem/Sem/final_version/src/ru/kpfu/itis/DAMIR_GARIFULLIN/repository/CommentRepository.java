package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Article;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;

/**
 * Created by Damir on 09.11.2015.
 */
public class CommentRepository {
    public static ArrayList getComments() throws SQLException {
        ArrayList arrayList = new ArrayList();
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("SELECT *  FROM comments ");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                ArrayList<String> files = new ArrayList<>();
                rs.getString("files");
                String[] fil = rs.getString("files").split(",");
                for (String s : fil){
                    files.add(s);
                }
                Comment comment = new Comment(rs.getInt("article_id"),rs.getString("author"),rs.getString("content"),files,rs.getDate("date_time"),rs.getInt("id"));
                arrayList.add(comment);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return arrayList;
    }
    public static void  addComment(int articleId,String author,  String content, ArrayList<String> files,Date dateTime) throws SQLException {
        try {
            Connection conn = MyConnection.getConnection();
            String filesS = "";
            for (String s : files){
                filesS += s;
            }
            PreparedStatement ps = conn.prepareStatement("INSERT INTO comment (article_id,author,files,content,date_time) VALUES (?,?,?,?,?)");
            ps.setInt(1, articleId);
            ps.setString(2, author);
            ps.setDate(5, dateTime);
            ps.setString(4, content);
            ps.setString(3, filesS);
            ps.execute();
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
    }
}
