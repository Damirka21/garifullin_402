package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Article;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Result;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Damir on 15.11.2015.
 */
public class ResultRepository {
    public static ArrayList<Result> getResults() throws SQLException {
        ArrayList<Result> arrayList = new ArrayList<>();
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("SELECT *  FROM results ");
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                Result result = new Result(rs.getString("team1"), rs.getString("team2"), rs.getInt("t1"), rs.getInt("t2"));
                arrayList.add(result);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return arrayList;
    }

}
