package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

import java.util.Map;

/**
 * Created by Damir on 09.11.2015.
 */
public class Voting {
    public String getQuestion() {
        return question;
    }

    public int getId() {
        return id;
    }

    public String getAnswer1() {
        return answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public int getA1() {
        return a1;
    }

    public int getA2() {
        return a2;
    }

    public int getA3() {
        return a3;
    }

    public int getA4() {
        return a4;
    }

    public Voting(String question, int id, String answer1, String answer2, String answer3, String answer4, int a1, int a2, int a3, int a4) {
        this.question = question;
        this.id = id;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;

        this.a1 = a1;
        this.a2 = a2;
        this.a3 = a3;
        this.a4 = a4;
    }

    String question;
    int id;
    String answer1;
    String answer2;
    String answer3;
    String answer4;
    int a1;
    int a2;
    int a3;
    int a4;

}
