package ru.kpfu.itis.DAMIR_GARIFULLIN.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Article;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.ArticleRepository;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.ConfigSingletone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Damir on 18.10.2015.
 */
@WebServlet(name = "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.main")
public class main extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        login.root.put("request",request.getRequestURI());
        Template tmpl = cfg.getTemplate("main.ftl");
        try {
            ArrayList<Article> arrayList = ArticleRepository.getArticles();
            login.root.put("articles",arrayList);
            tmpl.process(login.root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
