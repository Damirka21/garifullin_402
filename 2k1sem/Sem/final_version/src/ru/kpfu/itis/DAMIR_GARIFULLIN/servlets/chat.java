package ru.kpfu.itis.DAMIR_GARIFULLIN.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.kpfu.itis.DAMIR_GARIFULLIN.models.Message;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.ConfigSingletone;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.MessageRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Damir on 18.10.2015.
 */
@WebServlet(name = "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.chat")
public class chat extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(request.getParameter("chat_message") != null){
            try {
                MessageRepository.addMessage(request.getParameter("recipient"),(String)session.getAttribute("current_user"),
                        request.getParameter("chat_message"),new ArrayList<>());
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        response.sendRedirect("/chat");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        login.root.put("request",request.getRequestURI());
        HttpSession session = request.getSession();
        if (session.getAttribute("current_user") == null){
            response.sendRedirect("/login");
        }
        Template tmpl = cfg.getTemplate("chat.ftl");


        try {
            ArrayList<Message> arrayList = MessageRepository.getMessages();
            login.root.put("messages",arrayList);
            tmpl.process(login.root,response.getWriter());
        } catch (TemplateException | SQLException e) {
            e.printStackTrace();
        }
    }
}
