<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Online</title>
</head>

<body>
<#include "menu.ftl">
<ul>
<#list onlines as online>
    <div class="well">
        <li><a href="${online.getUrl()}" class="links">${online.getName()}</a></li>
    ${online.getDescription()}
    </div>
</#list>
</ul>
</body>
</html>
