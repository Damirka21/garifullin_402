<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../css/style.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap-responsive.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap-responsive.min.css"/>
    <script type="application/javascript" src="../../js/jquery-1.9.1.js"></script>
    <script type="application/javascript" src="../../js/bootstrap.js"></script>
    <script type="application/javascript" src="../../js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="navbar-header">
        <#if request?? && request?has_content>
            <h4>${request}</h4>
        <#else>
            <h4>Football portal</h4>
        </#if>
    </div>
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-right">
            <li><a  href="/main">Main</a></li>
            <li><a  href="/chat">Chat</a></li>
            <li><a  href="/register">Register</a></li>
            <li><a  href="/online">Online</a></li>
            <li><a  href="/results">Results</a></li>
            <li><a  href="/profile">Profile</a></li>
            <li><a href="/logout" class="logout">Logout</a></li>
        </ul>
    </div>

</nav>


</body>
</html>