<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Log in</title>
</head>
<body>
<#include "menu.ftl">

<#if username?? && username?has_content>
<form class="form-horizontal" method='post' action='/login'>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Login</label>
        <div class="controls">
            <input type="text" name='username' value="${username}" placeholder="Login">
        </div>
    </div>
<#else>
<form class="form-horizontal" method='post' action='/login'>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Login</label>
        <div class="controls">
            <input type="text" name='username' value="" placeholder="Login">
        </div>
    </div>
</#if>
    <div class="control-group">
        <label class="control-label" for="inputPassword">Password</label>
        <div class="controls">
            <input type="password"  name='password' placeholder="Password">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">Sign in</button>
        </div>
    </div>
</form>
</body>
</html>