<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Results</title>
</head>

<body>
<#include "menu.ftl">

<#list results as result>
<div class="well">
    <p>${result.getTeam1()} ${result.getT1()} : ${result.getT2()} ${result.getTeam2()}</p>
</div>
</#list>

</body>
</html>
