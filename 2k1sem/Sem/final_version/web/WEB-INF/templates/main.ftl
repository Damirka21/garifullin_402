<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap-responsive.css"/>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap-responsive.min.css"/>
    <script type="application/javascript" src="../../js/jquery-1.9.1.js"></script>
    <script type="application/javascript" src="../../js/bootstrap.js"></script>
    <script type="application/javascript" src="../../js/bootstrap.min.js"></script>
    <title>Main</title>
</head>

<body>

<#include "menu.ftl">
<#list articles  as article>
<div class="well">
    <article>
        <h3>${article.getName()}</h3>
        <div >
        <img src="${article.getFiles()}" class="imag">
        </div>
        ${article.getContent()}
        <p align="right"><i> written by ${article.getAuthor()}</i></p>

    </article>
    </div>
</#list>
</body>
</html>
