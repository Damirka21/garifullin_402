<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register</title>
</head>
<body>

<#include "menu.ftl">
<br>
<form method='post' action='/register' value="ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.register">
<#if message?? && message?has_content>
    <h5>${message}</h5>
<br>

</#if>
    Username: <input type='text' name='username' /><br>
    Password: <input type='password' name='password1'/><br>
    Repeat password: <input type='password' name='password2'/><br>
    Birthday : <input type="date" name="birthday"><br>
    E-mail : <input type='email' name='email'/><br>

    <input type='submit' value='Register'/><br>
</form>
</body>
</html>