<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Chat</title>
</head>

<body>
<#include "menu.ftl">

<#list messages as message>
    <p>${message.getSender()} => ${message.getRecipient()} : ${message.getContent()}</p>
</#list>
<form action="/chat" method="post">
    Send to:    <input type="text" name="recipient"><br>
    Text : <input type="text" name="chat_message">
    <input type="submit" value="Send">
</form>
</body>
</html>
