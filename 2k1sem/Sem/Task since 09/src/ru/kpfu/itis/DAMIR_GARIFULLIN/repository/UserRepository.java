package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import ru.kpfu.itis.DAMIR_GARIFULLIN.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Damir on 09.11.2015.
 */
public class UserRepository {
    public static HashSet getUsers() throws SQLException {
        HashSet hashSet = new HashSet();
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("SELECT *  FROM users ");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User(rs.getInt("id"),rs.getString("login"),rs.getString("password"),rs.getDate("bitrhday"),rs.getString("email"));
                hashSet.add(user);
            }
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        return hashSet;
    }
    public static void  addUser(String login, String password, Date birthday,String email) throws SQLException {
        try {
            Connection conn = MyConnection.getConnection();
            PreparedStatement ps = conn.prepareStatement("INSERT INTO users(login,password,birthday,email) VALUES (?,?,?,?)");
            ps.setString(1, login);
            ps.setString(2, password);
            ps.setDate(3, birthday);
            ps.setString(4, email);
            ps.execute();
            conn.close();
        } catch (SQLException | ClassNotFoundException e1) {
            e1.printStackTrace();
        }
    }
}
