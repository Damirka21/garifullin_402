package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

import java.util.ArrayList;

/**
 * Created by Damir on 09.11.2015.
 */
public class Article {
    int id;
    String name;
    String author;
    ArrayList<String> content;
    ArrayList<String> files;
}
