package ru.kpfu.itis.DAMIR_GARIFULLIN.servlets;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.ConfigSingletone;
import ru.kpfu.itis.DAMIR_GARIFULLIN.repository.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Damir on 29.09.2015.
 */
@WebServlet(name = "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.login")
public class login extends HttpServlet {
    static HashMap<String, Object> root = new HashMap<>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        HashMap hashMap = null;
        try {
            hashMap = UserRepository.getUsers();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Template tmpl = cfg.getTemplate("ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.login.ftl");
        if (user != null) {
            response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.profile");

        }
        else {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            boolean b = password.equals(hashMap.get(username));
            if (b) {
                session.setAttribute("current_user", username);
                root.put("username", null);
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.profile");
            }
            else {
                root.put("username",username);
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.login");

            }
        }
        try {
            tmpl.process(root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.login.ftl");
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");
        if (user != null) {
            response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.profile");
        }
            try {
                tmpl.process(root,response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }
    }

