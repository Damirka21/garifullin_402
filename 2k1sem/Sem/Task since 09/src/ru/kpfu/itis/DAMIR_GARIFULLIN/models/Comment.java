package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Damir on 09.11.2015.
 */
public class Comment {
    int articleId;
    String author;
    ArrayList<String> content;
    ArrayList<String> files;
    Date dateTime;
    int id;
}
