package ru.kpfu.itis.DAMIR_GARIFULLIN.models;

import java.util.Map;

/**
 * Created by Damir on 09.11.2015.
 */
public class Voting {
    String question;
    int id;
    String answer1;
    String answer2;
    String answer3;
    String answer4;
    Map<String,Integer> results;
}
