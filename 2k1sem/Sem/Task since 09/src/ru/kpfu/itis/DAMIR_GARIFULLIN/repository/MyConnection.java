package ru.kpfu.itis.DAMIR_GARIFULLIN.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Damir on 08.11.2015.
 */
public class MyConnection {
    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/sem",
                "postgres",
                "qwerty"
        );
        return conn;
    }
}
