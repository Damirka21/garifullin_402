package ru.kpfu.itis.DAMIR_GARIFULLIN.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Damir on 18.10.2015.
 */
@WebServlet(name = "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.menu")
public class menu extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String string = request.getParameter("ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.menu");
        switch (string) {
            case "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.main":
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.main");
                break;
            case "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.profile":
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.profile");
                break;
            case "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.results":
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.results");
                break;
            case "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.chat":
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.chat");
                break;
            case "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.store":
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.store");
                break;
            case "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.forum":
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.forum");
                break;
            case "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.online":
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.online");
                break;
            case "ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.voting":
                response.sendRedirect("/ru.kpfu.itis.DAMIR_GARIFULLIN.servlets.voting");
                break;
        }
    }
}
