delete from articles ;
INSERT INTO articles(name,author,content,files) values ('Chelsea win overshadowed by Jose Mourinho\'s choice to drop Eden Hazard!','Miguel Delaney',
	'After an easy win, Jose Mourinho could have given an easy answer to the only real contentious question of the day. 
	Chelsea had just beaten Aston Villa 2-0 to end what the Portuguese had described as the worst period of his career, but he had done so without his best player.
	 Eden Hazard had been left on the bench.
	 When inevitably asked about this, Mourinho could have rather simply said that the Belgian was rested or that it was just a temporary reshuffle. Instead, having seemed to close a problematic run of form,
	 Mourinho opened up some even more problematic public discussions. The Chelsea manager was brutally honest -- perhaps too honest for his own good. He somehow created unnecessarily complicated headlines and
	  potential future difficulties out of this easy win.
	"I left out Hazard because we are conceding lots of goals," Mourinho said after Saturday\'s win. 
	"We need to defend better. We need our midfielders to be just worried in the central area of the pitch, 
	not worrying about compensation on the left or right.
    "Playing with Willian and Pedro, the midfielders don\'t have to move left or right. They know those parts are controlled.
     Pedro and Willian did amazing work and allowed the midfielders to be very comfortable and have performances like Ramires\' 
     and [Cesc] Fabregas,\' controlling totally the centre of the pitch. It was a tactical decision. Leaving super quality on the bench,
      but bringing tactical discipline and hoping the team could be solid."
	Even allowing for the questions arising from his comments about Hazard\'s role in the team, Mourinho could have left it there.
	 He could have just said he needed to strengthen this "tactical discipline" to arrest the recent form, to steady the team. 
	 It would have been understandable.
	Instead, he doubled down. He implied Hazard hasn\'t been following instructions in the manner required and could well be left out again.
	 "I continue that way [with this lineup] or [Hazard] comes in our direction and tries to emulate the same work that 
	 Willian and Pedro put in."','');
INSERT INTO articles(name,author,content,files) values ('Pato back to Europe?','Sid Lowe',
	'It appears the possibility of Alexandre Pato making a return to Europe hasn\'t diminished after being linked
	 with a shock move to Manchester United during the summer. Marca is reporting that Man United remain interested
	  in bringing in the Brazilian forward, who currently plays with Sao Paulo.
	However, several other clubs are also interested, including Dortmund and Arsenal,
	 and if he wants to stay in Brazil, Corinthians have been tracking his status as well.
	After an injury-stricken time at AC Milan, it appears as if Pato could make a return to an elite club soon.
	 He has scored 125 goals in 322 appearances.','');

	 
	 
delete from comments ;
INSERT INTO comments(article_id,author,content,date_time,files) values (2,'user','IT IS VERY COOL!!!!11!!','2015-09-09',' ');
INSERT INTO comments(article_id,author,content,date_time,files) values (2,'Andrey','user, are you sure, bro?','2015-09-10',' ');
INSERT INTO comments(article_id,author,content,date_time,files) values (2,'user','Andrey, yes. I think that he will become a legend','2015-09-09',' ');

delete from users ;
INSERT INTO users(login,password,birthday,email) values ('admin','admin','1996-04-21','damir.garifullin96@gmail.com');
INSERT INTO users(login,password,birthday,email) values ('user','user','1996-04-21','efdodik@gmail.com');
INSERT INTO users(login,password,birthday,email) values ('Andrey','12345678','1994-10-10','Andrey.moderator@bk.ru');

delete from messages ;
INSERT INTO messages(recipient,sender,content,files) values('admin','Andrey','Andrey , thank you for registration in out web-site!',' ');
INSERT INTO messages(recipient,sender,content,files) values('admin','user','user, thank you for registration in out web-site!',' ');
INSERT INTO messages(recipient,sender,content,files) values('user','Andrey','Hi, dude!','https://www.google.ru/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0CAcQjRxqFQoTCLOi6Ij_y8gCFaK8cgodJowNHw&url=http%3A%2F%2Fwww.babyblog.ru%2Fuser%2Fflame_i%2F3124099&bvm=bv.105454873,d.bGQ&psig=AFQjCNEaFOes8ku9yOCvz_zNAmxOL3Yp_A&ust=1445256742055416');

delete from forum_articles;

INSERT INTO forum_articles(name,description,files) values('transfers','All news and gossips',' ');
INSERT INTO forum_articles(name,description,files) values('bets','Profitable bets, different forecasts',' ');
INSERT INTO forum_articles(name,description,files) values('offers and reviews','you think',' ');

delete from votings;
INSERT INTO votings(question,answer1,answer2,answer3,answer4,a1,a2,a3,a4) values('Who is the best forward?','Aguero','Suarez','Balotelli','Lewandowski',1,2,3,4);
INSERT INTO votings(question,answer1,answer2,answer3,answer4,a1,a2,a3,a4) values('Whi is the best goalkeeper','Cassilias','Chech','Neuer','De Gea',4,2,6,9);
INSERT INTO votings(question,answer1,answer2,answer3,answer4,a1,a2,a3,a4) values('Who is the best coach','Guardiola','Anchelotti','Sir Alex','Klopp',0,2,8,99);

delete from online;
INSERT INTO online(name,description,url) values ('Real Madrid - Barcelona','Today is El Classico!!','http://sport7.ru/match/123123123/real-madrid-barcelona');
INSERT INTO online(name,description,url) values ('Arsenal - Chelsea','London is red or blue?','http://sport7.ru/match/982461/arsenal-chelsea');
INSERT INTO online(name,description,url) values ('Argentina - Jamaica','Will we see 5-0?','http://sport7.ru/match/58917236182352639/argentina-jamaica');