CREATE TABLE articles(
    id serial,
    name VARCHAR(100),
    author  VARCHAR(30),
    content  text,
    files text
 );

CREATE TABLE comments(
    article_id   INT,
    author  VARCHAR(30),
    content   text,
    date_time date,
    id serial,
    files text);

CREATE TABLE users (
    login  VARCHAR(16),
    password VARCHAR(16),
    birthday   DATE,
    email  VARCHAR(30),
    id serial);

CREATE TABLE messages(
    recipient  VARCHAR(30),
    sender VARCHAR(30),
    content  text,
    files  text);

CREATE TABLE forum_articles(   
    name  VARCHAR(30),
    description   text,
    id serial,
    files   text);

CREATE TABLE votings (
    question  text,
    id serial,
    answer1  VARCHAR(30),
    answer3  VARCHAR(30),
    answer2  VARCHAR(30),
    answer4  VARCHAR(30),
    a1 int,
    a2 int,
    a3 int,
    a4 int);

CREATE TABLE online(
    name VARCHAR(30),
    description text,
    url text
);