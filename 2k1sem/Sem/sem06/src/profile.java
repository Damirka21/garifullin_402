import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Damir on 29.09.2015.
 */
@WebServlet(name = "profile")
public class profile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String user = (String)session.getAttribute("current_user");
        if(user != null){
            PrintWriter pw = response.getWriter();
            response.setContentType("text/html");
            pw.println("Hi, " + user);
            pw.println("<form method='get' action='/logout'>");
            pw.println("<input type='submit' value='logout' />");
            pw.println("</form>");
            pw.close();
        }else{
            response.sendRedirect("/login");
        }
    }
}
