import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by Damir on 29.09.2015.
 */
@WebServlet(name = "login")
public class login extends HttpServlet {
    HashMap<String,String> hashMap = null;
    @Override
    public void init() throws ServletException {
        super.init();
        hashMap = new HashMap<>();
        hashMap.put("admin", "admin");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");
        if (user != null) {
            response.sendRedirect("/profile");

        }
        else {

            String username = request.getParameter("username");
            String password = request.getParameter("password");
            boolean b = password.equals(hashMap.get(username));
            if (b) {
                session.setAttribute("current_user", username);
                if(request.getParameter("checked").equals("checked") ){
                    Cookie cookie = new Cookie("name", "value");
                    cookie.setMaxAge(24 * 60 * 60);
                    response.addCookie(cookie);
                }
                response.sendRedirect("/profile");
            }
            else {
                response.sendRedirect("/login?username=" + username);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");
        response.setContentType("text/html");
        if (user != null) {
            response.sendRedirect("/profile");
        }
        else {
            PrintWriter pw = response.getWriter();
            String username = request.getParameter("username");
            if(username != null){
                pw.println("<p>Invalid username or password</p>");
                pw.println("<form method='post' action='/login'>");
                pw.println("Username: <input type='text' name='username'");
                pw.print("value=" + username);
                pw.print(" />");
            }
            else {
                pw.println("<form method='post' action='/login'>");
                pw.println("Username: <input type='text' name='username' />");
            }

            pw.println("<br>");
            pw.println("Password: <input type='text' name='password' />");
            pw.println("<br>");
            pw.println("<input type=\"checkbox\" name=\"checked\"><span>��������� ����</span><br>");
            pw.println("<input type='submit' value='Log In' />");
            pw.println("</form>");
            pw.close();
        }
    }
}
