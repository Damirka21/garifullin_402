import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Damir on 18.10.2015.
 */
@WebServlet(name = "menu")
public class menu extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String string = request.getParameter("menu");
        switch (string) {
            case "main":
                response.sendRedirect("/main");
                break;
            case "profile":
                response.sendRedirect("/profile");
                break;
            case "results":
                response.sendRedirect("/results");
                break;
            case "chat":
                response.sendRedirect("/chat");
                break;
            case "store":
                response.sendRedirect("/store");
                break;
            case "forum":
                response.sendRedirect("/forum");
                break;
            case "online":
                response.sendRedirect("/online");
                break;
            case "voting":
                response.sendRedirect("/voting");
                break;
        }
    }
}
