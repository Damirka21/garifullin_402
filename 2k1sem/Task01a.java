package Homework;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by @Damir
 * on  21.09.2015
 * *
 */
public class Task01a {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("(([\\-+]?[1-9][0-9]*)\\,[0-9]*([(][0-9]*[1-9][)][0-9]*)?[1-9])|(([\\-+]?[1-9][0-9]*)\\,[0-9]*([(][0-9]*[1-9][)]))|0|([\\-+]?0\\,[0-9]*([(][0-9]*[1-9][)]))|([\\-+]?0\\,[0-9]*([(][0-9]*[1-9][)][0-9]*)?[1-9])|([\\-+][1-9][0-9]*)");
        Matcher matcher = pattern.matcher("0,(35)0");
        System.out.println(matcher.matches());
    }
}
