package Homework;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Scanner;
import java.util.regex.*;


/**
 * Created by @Damir
 * on  23.09.2015
 * *
 */

public class Task09b {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        while (!string.equals("/exit")) {
            long date = new Date().getTime();
            PrintWriter pw = new PrintWriter(new File(date + "output.html"));
            pw.println("<!DOCTYPE html><html><head><title>" + string +
                    "</title></head><body>");
            Pattern p = Pattern.compile("/(getdate|((add|mult)/(0|[1-9][0-9]*)/(0|[1-9][0-9]*))|(((baidu|bing|yahoo|aol)\\.com)/search))");
            Matcher m = p.matcher(string);
            if (m.matches()) {
                if (string.equals("/getdate")) {
                    pw.println(new Date().toString());
                }
                Pattern p1 = Pattern.compile("/(add|mult)/(0|[1-9][0-9]*)/(0|[1-9][0-9]*)");
                Matcher m1 = p1.matcher(string);
                if (m1.matches()) {
                    boolean f = m1.group(1).equals("mult");
                    int a1 = Integer.parseInt(m1.group(2));
                    int a2 = Integer.parseInt(m1.group(3));
                    if (f= true) {
                        pw.println(a1 * a2);
                    }
                    else {
                        pw.println(a1 + a2);
                    }
                }
                if (string.endsWith("search")) {
                    System.out.println(m.group(1));
                    String search = m.group(1);
                    String text = "q";
                    pw.print("<form action=\"http");
                    if (search.equals("baidu.com/search")) {
                        text = "wd";
                        pw.print("s://" + search + "/s\"");
                    }
                    if (search.equals("bing.com/search")) {
                        text = "q";
                        pw.print("://" + search + "/search\"");
                    }
                    if (search.equals("yahoo.com/search")) {
                        text = "p";
                        pw.print("s://search." + search + "/search\"");
                    }
                    if (search.equals("aol.com/search")) {
                        text = "q";
                        pw.print("://search." + search + "/aol/search\"");
                    }
                    pw.println(" method=\"GET\">");
                    pw.println("<input type=\"text\" name=\"" + text + "\">");
                    pw.println("<input type=\"submit\" value=\"search on " + search + "\">\n</form>");
                }
            } else {
                pw.println("Error404 - Not Found Exception");
            }
            pw.println("</body></html>");
            pw.close();
            string = scanner.nextLine();
        }
    }
}
