package Homework;

import java.util.Random;
import java.util.regex.*;

/**
 * Created by @Damir
 * on  20.09.2015
 * *
 */
public class Task03c {
    public static void main(String[] args) {
        Random random = new Random();
        int count = 0, p = 0;
        Pattern pattern = Pattern.compile("[02468]$");
        while (p < 10) {
            int a = random.nextInt(Integer.MAX_VALUE);
            String check = String.valueOf(a);
            Matcher matcher = pattern.matcher(check);
            if (a > 0 && matcher.matches()) {
                p++;
                System.out.println(a);
            }
            count ++;
        }
        System.out.println(count);
    }
}