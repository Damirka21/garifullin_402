import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Damir on 8.11.15.
 */
public class StudentSearchServlet extends HttpServlet{


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String q = request.getParameter("q");
        String var = request.getParameter("var");
        try {
            PreparedStatement ps = MyConnection.getConnection().prepareStatement(
                    "select name from " + var + " where name like ?"
            );
            ps.setString(1, "%" + q + "%");
            ResultSet rs = ps.executeQuery();
            JSONArray ja = new JSONArray();
            while (rs.next()){
                ja.put(rs.getString("name"));
            }
            JSONObject jo = new JSONObject();
            jo.put("results", ja);
            response.getWriter().print(jo.toString());

        } catch (SQLException | JSONException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
