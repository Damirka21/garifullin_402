<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
    <script type="application/javascript" src="/js/jquery-1.11.3.js"></script>
    <script type="application/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
</head>

<body>
<select id="var">
    <option name="teachers">teachers</option>
    <option name="students">students</option>
    <option name="classes">classes</option>
</select>
<input type="text" id="s" oninput="f()"/>

<div id="res"></div>

<script type="application/javascript">

    f = function (req, resp) {
        if ($("#s").val().length > 0) {
            $.ajax({
                url: "/search",
                data: {"q": $("#s").val(), "var": $("#var").val()},
                dataType: "json",
                success: function (resp) {
                    if (resp.results.length > 0) {
                        var availableTags = resp.results;
                        $("#s").autocomplete({
                            source: availableTags
                        });
                        $("#res").text("Yes results");
                        for (var i = 0; i < resp.results.length; i++) {
                            $("#res").append("<li>" + resp.results[i] + "</li>");
                        }
                    } else {
                        $("#res").text("No results");
                    }
                }
            })
        } else {
            $("#res").text("");
        }
    }
</script>
</body>
</html>
