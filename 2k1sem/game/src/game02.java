import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Damir on 22.11.2015.
 */
public class game02 extends JFrame {
    private int buttonSize = 50;
    private int[][] yourField;
    private int[][] enemyField;
    JButton[][] yourGameField;
    JButton[][] enemyGameField;
    boolean[][] checked;
    public int shipCount ;


    public game02() {
        yourGameField = new JButton[11][11];
        enemyGameField = new JButton[11][11];
        setLayout(new GroupLayout(getContentPane()));
        shipCount = 0;
        this.setLayout(new GroupLayout(getContentPane()));

        setBounds(50, 50, buttonSize * 23 + 50, buttonSize * 23 + 50);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        String[] array = {"","A","B","C","D","E","F","G","H","I","J"};
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
                if(i == 0 && j == 0){
                    yourGameField[i][j] = new JButton("Y");
                    yourGameField[i][j].setEnabled(false);
                    enemyGameField[i][j] = new JButton("E");
                    enemyGameField[i][j].setEnabled(false);

                }else if (j == 0){
                    yourGameField[i][j] = new JButton(array[i]);
                    enemyGameField[i][j] = new JButton(array[i]);
                }else if(i == 0){
                    yourGameField[i][j] = new JButton(j+"");
                    enemyGameField[i][j] = new JButton(j+"");
                }else {
                    yourGameField[i][j] = new JButton("");
                    enemyGameField[i][j] = new JButton("");
                }
                yourGameField[i][j].setBounds(i * buttonSize, j * buttonSize, buttonSize, buttonSize);
                enemyGameField[i][j].setBounds((i + 12 )* buttonSize, (j) * buttonSize, buttonSize, buttonSize);
                add(yourGameField[i][j]);
                add(enemyGameField[i][j]);
            }
        }

        setVisible(true);

    }

    public static void main(String[] args) {
        game02 game = new game02();
        for ( int i = 1; i < 11; i ++){
            for (int j = 1; j < 11; j ++){
                game.enemyGameField[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JButton jb = (JButton) e.getSource();
                        System.out.println(jb.getX()/50 - 12);
                        System.out.println(jb.getY()/50);
                    }
                });
            }
        }
    }


}
