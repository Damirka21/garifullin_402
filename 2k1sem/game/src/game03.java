import java.util.Scanner;

/**
 * Created by Damir on 22.11.2015.
 */
public class game03 {
    static boolean[][] player1 = new boolean[10][10];
    static int[][] player2 = new int[10][10];
    static boolean[][] player2Shoots = new boolean[10][10];
    static Scanner scanner = new Scanner(System.in);

    public static void setPlayer1() {

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4 - i; j++) {
                System.out.println("Coordinates of " + (i + 1) + " squares boat in '(A-Z):(01-10)-(A-Z):(01-10)' format");
                String input = scanner.nextLine();
                //Parsing data
                int a = (Integer.parseInt(input.charAt(2) + "" + input.charAt(3))) - 1;
                int b = (Integer.parseInt(input.charAt(7) + "" + input.charAt(8))) - 1;

                for (int i1 = a; i1 < b + 1; i1++) {
                    for (int j1 = (input.charAt(0) - (int) 'A'); j1 < (input.charAt(5) - (int) 'A') + 1; j1++) {
                        player1[i1][j1] = true;
                    }
                }


            }
        }
    }

    public static void main(String[] args) {
        //setPlayer1();
        int count = 0;
        for(int i = 0; i < 10;i ++){
            player1[i][0] = true;
            player1[i][1] = true;
        }
        while (count < 20) {
            System.out.print("Your step:");
            String input = scanner.nextLine();

            int a = (Integer.parseInt(input.charAt(2) + "" + input.charAt(3))) - 1;
            int b = input.charAt(0) - (int) 'A';

            if (player1[a][b] && !player2Shoots[a][b]) {
                player2[a][b] = 1;
                player2Shoots[a][b] = true;
                System.out.println("Good shoot!");
                count++;
            } else if (player2Shoots[a][b]) {
                System.out.println("You already shoot to this square");
            } else {
                System.out.println("Missing");
            }
        }
    }
}



