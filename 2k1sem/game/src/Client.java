
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Damir on 26.11.2015.
 */
public class Client extends JFrame {
    private boolean shootable;
    private boolean iAmReady = false;
    public static int showMessage() {
        JFrame myFrame = new JFrame("showOptionDialog() Test");
        myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container myPane = myFrame.getContentPane();
        int messageType = JOptionPane.QUESTION_MESSAGE;
        String message = "Are you ready?";
        String[] options = {"Yes", "No"};
        int code = JOptionPane.showOptionDialog(myFrame,
                message,
                "Option Dialog Box", 0, messageType,
                null, options, "PHP");

        if (code == 0) {
            return 1;
        } else {
            return -1;
        }
    }

    public Client() throws IOException {
        final int PORT = 3456;
        final String HOST = "localhost";
        Socket s = new Socket(HOST, PORT);
        BufferedReader is = new BufferedReader(
                new InputStreamReader(
                        s.getInputStream()
                )
        );

        PrintWriter os = new PrintWriter(
                s.getOutputStream(), true);
        game02 game = new game02();
        for (int i = 1; i < 11; i++) {
            for (int j = 1; j < 11; j++) {
                game.enemyGameField[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (shootable && iAmReady) {
                            JButton jb = (JButton) e.getSource();
                            System.out.println("I am shooting");
                            if (jb.isEnabled()) {
                                System.out.println("I am shooting x 2");
                                os.println();
                                os.println(jb.getX() / 50 - 12);
                                os.println(jb.getY() / 50);
                                jb.setEnabled(false);
                            }
                        }
                    }
                });
            }
        }

        for (int i = 1; i < 11; i++) {
            for (int j = 1; j < 11; j++) {
                game.yourGameField[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (!iAmReady) {
                            JButton jb = (JButton) e.getSource();
                            if (game.shipCount < 20) {
                                if (jb.getText().equals("")) {
                                    game.shipCount++;
                                    jb.setText("x");
                                } else if (jb.getText().equals("x")) {
                                    game.shipCount--;
                                    jb.setText("");
                                }
                            } else if (game.shipCount == 20) {
                                if (jb.getText().equals("x")) {
                                    game.shipCount--;
                                    jb.setText("");
                                } else {
                                    if (showMessage() > 0) {
                                        iAmReady = true;
//                                        os.println(1);
                                    }
                                }

                            } else {
                                if (showMessage() > 0) {
                                    iAmReady = true;
//                                    os.println(1);
                                }
                            }
                        }
                    }
                });
            }
        }
        int whoIs = 0;
        while (whoIs != -1 && whoIs != 1) {
            whoIs = Integer.parseInt(is.readLine());
            if (whoIs > 0) {
                shootable = true;
            } else {
                shootable = false;
            }
            System.out.println(whoIs);
        }
//        int enemyReady = -1;
//        while (enemyReady < 0) {
//            if (s.getInputStream().available() > -1) {
//                enemyReady = Integer.parseInt(is.readLine());
//                System.out.println(enemyReady);
//            }
//        }
        while (true) {

                if (!shootable) {
                    System.out.println("I am ready to def");
                    is.readLine();
                    int a = Integer.parseInt(is.readLine());
                    int b = Integer.parseInt(is.readLine());
                    if (game.yourGameField[a][b].getText().equals("x")) {
                        os.println(1);
                        System.out.println("FUCK");
                    } else {
                        os.println(-1);
                        System.out.println("LUCK");
                        shootable = true;
                    }
                    os.println(a);
                    os.println(b);
                } else {
                    System.out.println("I am ready to shoot");
                    int c = Integer.parseInt(is.readLine());
                    int a = Integer.parseInt(is.readLine());
                    int b = Integer.parseInt(is.readLine());
                    if (c > 0) {
                        game.enemyGameField[a][b].setText("x");
                        System.out.println("shooted");
                    } else {
                        game.enemyGameField[a][b].setText("");
                        shootable = false;
                        System.out.println("missed");
                    }
                }
        }

    }

    public static void main(String[] args) throws IOException {
        new Client();
    }
}
