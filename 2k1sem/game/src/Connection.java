import java.io.*;
import java.net.Socket;

/**
 * Created by Damir on 26.11.2015.
 */
class Connection implements Runnable {
    Socket socket1;
    Socket socket2;
    Thread thread1;
    Server server;
    public Connection(Server server, Socket socket1,Socket socket2) {
        this.socket1 = socket1;
        this.socket2 = socket2;
        this.server = server;
        thread1 = new Thread(this);
        thread1.start();

    }

    public void run() {
        try {
            InputStream is1 = socket1.getInputStream();
            BufferedReader br1 =  new BufferedReader(new InputStreamReader(is1));
            PrintWriter os1 = new PrintWriter(socket1.getOutputStream(), true);
            InputStream is2 =socket2.getInputStream();
            BufferedReader br2 =  new BufferedReader(new InputStreamReader(is2));
            PrintWriter os2 = new PrintWriter(socket2.getOutputStream(), true);
            os1.println("1");
            os2.println("-1");
            while(true){
                if(is1.available() > -1){
                    os2.println(br1.readLine());
                    os2.println(br1.readLine());
                    os2.println(br1.readLine());
                }
                if (is2.available() > -1){
                    os1.println(br2.readLine());
                    os1.println(br2.readLine());
                    os1.println(br2.readLine());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
