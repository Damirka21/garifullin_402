import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.ServletContext;

/**
 * Created by Damir on 08.10.2015.
 */
public class ConfigSingletone {
    private static Configuration cfg;

    public static Configuration getConfig(ServletContext sc) {
        if (cfg == null) {
            cfg = new Configuration(Configuration.VERSION_2_3_22);
            cfg.setServletContextForTemplateLoading(
                    sc,
                    "WEB-INF/templates"
            );
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
            //creating instance of Configuration

        }
        return cfg;
    }
}

