import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
        import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by Damir on 03.10.2015.
 */
@WebServlet(name = "process")
public class process extends HttpServlet {

    static HashMap<String, Object> root = new HashMap<>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String text = request.getParameter("text");
        int result = 0;
        root.put("text", request.getParameter("text"));
        switch (request.getParameter("process")) {
            case ("symbols"):
                for (int i = 0; i < text.length(); i++) {
                    if(text.charAt(i)!=' '){
                        result++;
                    }
                }
                break;
            case ("words"):
                String[] words = text.split(" ");
                result = words.length;
                break;
            case ("sentences"):
                String[] sentences = text.split("[!\\?\\.]");
                result = sentences.length;
                break;
            case ("paragraphs"):
                String[] parag = text.split("\\n");
                result = parag.length;
                break;
        }
        root.put("result", result);
        response.sendRedirect("/result");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("process.ftl");
        root.put("form_url",request.getRequestURI());

        try {
            tmpl.process(root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
