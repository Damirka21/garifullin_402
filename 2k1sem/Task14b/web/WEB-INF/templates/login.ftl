<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<#if username?? && username?has_content>
<p>Invalid username or password</p>
<form method='post' action='/login' value="${username}">
<#else>
<form method='post' action='/login'">
</#if>
    Username: <input type='text' name='username' />
    <br>
    Password: <input type='text' name='password' />
    <br>
    <input type="checkbox" name="checked"><span>Save me</span><br>
    <br>
    <input type='submit' value='Log In' />
</form>
</body>
</html>