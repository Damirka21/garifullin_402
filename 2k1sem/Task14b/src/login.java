import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by Damir on 29.09.2015.
 */
@WebServlet(name = "login")
public class login extends HttpServlet {
    HashMap<String,String> hashMap = null;
    @Override
    public void init() throws ServletException {
        super.init();
        hashMap = new HashMap<>();
        hashMap.put("admin", "admin");
    }

    static HashMap<String, Object> root = new HashMap<>();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");
        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("login.ftl");
        if (user != null) {
            response.sendRedirect("/profile");

        }
        else {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            boolean b = password.equals(hashMap.get(username));
            if (b) {
                session.setAttribute("current_user", username);
                root.put("username", null);
                response.sendRedirect("/profile");
            }
            else {
                root.put("username",username);
                response.sendRedirect("/login");

            }
        }
        try {
            tmpl.process(root,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Configuration cfg = ConfigSingletone.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("login.ftl");
        HttpSession session = request.getSession();
        String user = (String) session.getAttribute("current_user");
        if (user != null) {
            response.sendRedirect("/profile");
        }
            try {
                tmpl.process(root,response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }
    }

