package Homework;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by @Damir
 * on  20.09.2015
 * *
 */
public class Task06c {
    public static void main(String[] args) throws IOException {
        Pattern pattern = Pattern.compile("http://.*/([a-zA-Z_0-9-]*)\\.([a-zA-Z][a-zA-Z]*)");
        File file = new File("example.html");
        BufferedReader reader  = new BufferedReader(new FileReader(file));
        String line = "";
        while ((line = reader.readLine()) != null){
            Matcher matcher = pattern.matcher(line);
            while(matcher.find()){
                System.out.println("FILENAME : " + matcher.group(1) );
                System.out.println("EXTENSION : " + matcher.group(2) );
            }
        }
        reader.close();
    }
}
