import java.util.HashSet;

/**
 * Created by Damir on 09.11.2015.
 */
public class Cities {
    public  HashSet<String> cities  = new HashSet<String>();

    public  String lastCity;


    public  boolean contains(String city){
        return this.cities.contains(city);
    }

    public  boolean check(String city){
        return ( city.charAt(0) == this.lastCity.charAt(this.lastCity.length()-1));
    }
}
