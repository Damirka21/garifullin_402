import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Scanner;

public class Server {

    static Cities game;

    public static void main(String[] args) throws IOException {
        final int PORT = 3456;
        ServerSocket ss = new ServerSocket(PORT);
        System.out.println("Starting server");
        Socket s = ss.accept();
        System.out.println("Connected!");

        PrintWriter os = new PrintWriter(
                s.getOutputStream(), true);
        BufferedReader is = new BufferedReader(
                new InputStreamReader(
                        s.getInputStream()
                )
        );
        Scanner scanner = new Scanner(System.in);


        game = new Cities();
        System.out.println("What is your name? ");
        String serverName = scanner.nextLine();
        os.println(serverName);
        String clientName = "";
        while (clientName.equals("")) {
            clientName = is.readLine();
        }
        System.out.print(serverName + ": ");
        String x = scanner.nextLine();
        game.cities.add(x);
        game.lastCity = x;
        os.println(x);
        while (true) {
            x = is.readLine();
            game.lastCity = x;
            game.cities.add(x);
            System.out.println(clientName + ": " + x);
            System.out.print(serverName + ": ");
            x = scanner.nextLine();
            while(!game.check(x) || game.contains(x)) {
                if (!game.check(x)){
                    System.out.println("Wrong!");
                } else if (game.contains(x)) {
                    System.out.println("City is already used!");
                }
                System.out.print(serverName + ": ");
                x = scanner.nextLine();
            }
            game.cities.add(x);
            os.println(x);
        }
    }
}
