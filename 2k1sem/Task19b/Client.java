import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by ma on 29.10.2015.
 */
public class Client {
    static Cities game;
    public static void main(String[] args) throws IOException {
        final int PORT = 3456;
        final String HOST = "localhost";
        Socket s = new Socket(HOST, PORT);

        PrintWriter os = new PrintWriter(
                s.getOutputStream(), true);
        BufferedReader is = new BufferedReader(
                new InputStreamReader(
                        s.getInputStream()
                )
        );
        game = new Cities();
        Scanner scanner = new Scanner(System.in);
        System.out.println("What is your name? ");
        String clientName = scanner.nextLine();
        os.println(clientName);
        String serverName = "";
        while (serverName.equals("")) {
            serverName = is.readLine();
        }
        int i = 0;
        while (true) {
            String x = is.readLine();
            game.lastCity = x;
            game.cities.add(x);
            System.out.println(serverName + ": " + x);
            System.out.print(clientName + ": ");
            x = scanner.nextLine();
            while(!game.check(x) || game.contains(x)) {
                if (!game.check(x)){
                    System.out.println("Wrong!");
                } else if (game.contains(x)) {
                    System.out.println("City is already used!");
                }
                System.out.print(clientName + ": ");
                x = scanner.nextLine();
            }
            game.cities.add(x);
            os.println(x);
        }
    }
}
