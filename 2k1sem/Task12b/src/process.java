import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
        import java.io.PrintWriter;

/**
 * Created by Damir on 03.10.2015.
 */
@WebServlet(name = "process")
public class process extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String name = request.getParameter("name");
        int result = 0;
        switch (request.getParameter("process")) {
            case ("symbols"):
                for (int i = 0; i < name.length(); i++) {
                    if(name.charAt(i)!=' '){
                        result++;
                    }
                }
                break;
            case ("words"):
                String[] words = name.split(" ");
                result = words.length;
                break;
            case ("sentences"):
                String[] sentences = name.split("[!\\?\\.]");
                result = sentences.length;
                break;
            case ("paragraphs"):
                String[] parag = name.split("\\n");
                result = parag.length;
                break;
        }
        session.setAttribute("result", result);
        response.sendRedirect("/result");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        pw.println("<form method='post' action='/process'>");
        pw.println("<textarea rows='30' cols='50' name='name'  /></textarea>");
        pw.println("<select name='process'><option value='symbols'>symbols</option><option value='words'>words</option>");
        pw.print("<option value='sentences'>sentences</option><option value='paragraphs'>paragraphs</option>\"/select>");
        pw.println("<input type='submit' value='request' />");
        pw.println("</form>");
        pw.close();
    }
}
