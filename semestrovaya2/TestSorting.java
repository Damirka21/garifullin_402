package semestrovaya2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by Damir on 05.04.2015.
 */
public class TestSorting {
    public static void main(String[] args) throws FileNotFoundException {
        PrintWriter iteratorWriter = new PrintWriter(new FileOutputStream("Iterator.txt"));
        PrintWriter timeWriter = new PrintWriter(new FileOutputStream("Time.txt"));
        for (int i = 50; i <= 10000; i+= 50) {
            String name = "Generated" + i + ".txt";
            Scanner scanner = new Scanner(new File(name));
            PatienceSorting patienceSorting = new PatienceSorting();
            long a = System.currentTimeMillis();
            patienceSorting.sort(i,patienceSorting.reading(new File(name)));
            long b = System.currentTimeMillis() - a;
            timeWriter.println(b);
            iteratorWriter.println(patienceSorting.iterator);
        }
        iteratorWriter.close();
        timeWriter.close();
    }
}
