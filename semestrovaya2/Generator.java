package semestrovaya2;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Created by Damir on 05.04.2015.
 */
public class Generator {
    public static void main(String[] args) throws FileNotFoundException {
        Random random = new Random();
        for (int i = 50; i <= 10000; i+= 50) {
            String name = "Generated" + i + ".txt";
            PrintWriter writer = new PrintWriter(new FileOutputStream(name));
            for (int j = 0; j < i; j++)
                writer.print(random.nextInt() + " ");
            writer.close();
        }

    }
}
