package semestrovaya2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by Damir on 29.03.2015.
 */
public class PatienceSorting {
    public static HashSet<Stack<Integer>>  reading(File file) throws FileNotFoundException {

        Scanner scanner = new Scanner(file);
        HashSet<Stack<Integer>> myHashSet = new HashSet<>();
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        Stack<Integer> example = new Stack<>();
        example.push(a);
        while (scanner.hasNext()) {
            if (b < a) {
                example.push(b);
            } else {
                myHashSet.add(example);
                example = new Stack<>();
                example.push(b);
            }
            a = b;
            b = scanner.nextInt();
        }
        if (b < a) {
            example.push(b);
            myHashSet.add(example);
        } else {
            myHashSet.add(example);
            example = new Stack<>();
            example.push(b);
            myHashSet.add(example);
        }
        return myHashSet;
    }
    public static int[]  sort(int a,HashSet<Stack<Integer>> myHashSet){
        int[] result = new int[a];
        for (int i = 0; i < a; i++) {
            int min = 2147483647;
            Stack<Integer> needed = null;
            for (Stack<Integer> c : myHashSet) {
                if (!c.isEmpty() )
                    if (min > c.peek()) {
                        needed = c;
                        min = c.peek();
                    }
                iterator ++;
            }
            result[i] = needed.pop();
        }
        return result;
    }
    static int iterator = 0;
    public static void main(String[] args) throws FileNotFoundException {

    }

}
