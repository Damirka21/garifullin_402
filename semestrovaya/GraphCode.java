package semestrovaya;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Damir on 24.02.2015.
 */
public class GraphCode {
    public static Elem head = null, p;

    static Scanner scanner = new Scanner(System.in);

    public static int length,countEdges;

    public int[][] arr = getMI();

    public static void GraphCode(int [][] mi){
        countEdges = mi[0].length;
        length = mi.length;
        int two = 0;
        for (int j = 0; j < countEdges; j ++){
            for (int i = 0; i < length; i ++){
                if (two == 0) {
                    p = new Elem(0, 0, head);
                    if (mi[i][j] == 1) {
                        p.setValue1(i);
                        two++;
                    }else{
                        if (mi[i][j] == 2){
                            p.setValue1(i);
                            p.setValue2(i);
                            head = p;
                        }
                    }
                }else{
                    if (mi[i][j] == 1) {
                        p.setValue2(i);
                        two = 0;
                        head = p;
                    }
                }
            }
        }
    }   //complete


    public static void numbers(){
        p = head;
        countEdges = 0;
        length = 0;
        while (p != null){
            countEdges ++;
            if (p.getValue1()>length){
                length = p.getValue1();
            }
            if (p.getValue2()>length){
                length = p.getValue2();
            }
            p = p.getNext();
        }
        length ++;
    }   //additional

    public static   int[][] getMI(){
        numbers();
        int[][] mi = new int[length][countEdges];
        p = head;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < countEdges; j++) {
               mi[i][j] = 0;
            }
        }
        int i = 0;
        while (p != null){
            mi[p.getValue2()][i] += 1;
            mi[p.getValue1()][i] += 1;
            p = p.getNext();
            i ++;
        }
        return mi;
    }   //complete

    public static void insert(int i,int j){

        p =head;
        Elem p1 = new Elem(i,j,p.getNext());
        p.setNext(p1);
    }   //complete

    public static void delete(int i, int j){
        p = head;
        if (p.getNext().getValue2() == i || p.getNext().getValue1() == i){
            if (p.getNext().getValue2() == j || p.getNext().getValue1() == j){
                p.setNext(p.getNext().getNext());
                return;
            }
        }
        p = p.getNext();
    }   //complete

    public void modify(int i){
        while (p.getNext() != null){
            while (p.getNext().getValue2() == i || p.getNext().getValue1() == i) {
                p.setNext(p.getNext().getNext());
            }
                p = p.getNext();

        }
    }   //complete

    public static void graphToString(){
        p = head;
        for (int i = 0; i < length; i++) {
            System.out.println(p.getValue1() + " <-> " + p.getValue2() );
            p = p.getNext();
        }
    }   //complete

    public static GraphCode  getEdges(int i){
        GraphCode graphCode = new GraphCode();
        while (p != null){
            if(p.getValue1() == i || p.getValue2() == i){
                graphCode.insert(p.getValue1(),p.getValue2());
            }
            p = p.getNext();
        }
        return graphCode;
    }   //complete

    public static ArrayList<Integer> show (int m){
        ArrayList arrayList = new ArrayList();
        int[][] b = getMI();
        for (int i = 0; i < length; i ++){
            int sum = 0;
            for (int j = 0; j < countEdges; j ++){
                    sum += b[i][j];
            }
            if (sum > m){
                arrayList.add(i);
            }
        }
        return arrayList;
    }   //complete
}
