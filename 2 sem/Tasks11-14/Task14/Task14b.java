package Tasks11_14.Task14;

import Tasks11_14.Task13.Elem;
import Tasks11_14.Task13.MyLinkedStack;

import java.util.Scanner;

/**
 * Created by Damir on 10.03.2015.
 */
public class Task14b {
    public static void main(String[] args) {
        MyLinkedStack stack = new MyLinkedStack();
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        Elem<Character> test = new Elem();
        for (int i = 0; i < a.length(); i++) {
            char c = a.charAt(i);
            switch (c) {
                case '{':
                    stack.push('{');
                    break;
                case '[':
                    stack.push('[');
                    break;
                case '(':
                    stack.push('(');
                    break;
                case ')':
                    test = (Elem) stack.pop();
                    if (test != null){
                    if (test.getValue() != '(') {
                        if (stack.isEmpty()) {
                            System.out.println("Extra closing bracket");
                            return;
                        } else {
                            System.out.println("Brackets don't much each other");
                            return;
                        }
                    }
                    break;
                    }else {
                        System.out.println("Extra closing bracket");
                        return;
                    }
                case '}':
                    test = (Elem) stack.pop();
                    if(test  != null) {
                        if (test.getValue() != '{') {
                            if (stack.isEmpty()) {
                                System.out.println("Extra closing bracket");
                                return;
                            } else {
                                System.out.println("Brackets don't much each other");
                                return;
                            }
                        }
                        break;
                    }else{
                        System.out.println("Extra closing bracket");
                        return;
                    }
                case ']':
                    test = (Elem) stack.pop();
                    if(test  !=  null) {
                        if (test.getValue() != '[') {
                            if (stack.isEmpty()) {
                                System.out.println("Extra closing bracket");
                                return;
                            } else {
                                System.out.println("Brackets don't much each other");
                                return;
                            }
                        }
                        break;

                    }else{
                        System.out.println("Extra closing bracket");
                        return;
                    }
                default:  break;
            }
        }
        if (!stack.isEmpty()) {
            System.out.println("Not all opened brackets closed");
        } else {
            System.out.println("Well done!!!");
        }
    }
}
