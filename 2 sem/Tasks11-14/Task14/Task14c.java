package Tasks11_14.Task14;

import Tasks11_14.Task13.Elem;
import Tasks11_14.Task13.MyLinkedStack;

/**
 * Created by Damir on 10.03.2015.
 */
public class Task14c extends MyLinkedStack<Integer>{
    public static void main(String[] args) {
        MyLinkedStack  stack = new MyLinkedStack();
        int[] a =  {1,2,3,4,5};
        for (int i = 0; i < a.length; i ++){
            stack.push(a[i]);
        }
        for (int i = 0; i < a.length; i ++){
            Elem b = (Elem)stack.pop();
            a[i] = (Integer)b.getValue();
        }
        System.out.println(a[0]);
    }
}
