package Tasks11_14.Task14;

import Tasks11_14.Task13.Elem;
import Tasks11_14.Task13.MyLinkedStack;

import java.util.Scanner;

/**
 * Created by Damir on 10.03.2015.
 */
public class Task14a {

    public static boolean check(String string) {
        try {
            Integer.parseInt(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static int number(String string){
        return   Integer.valueOf(string);

    }
    public static void main(String[] args) {
        MyLinkedStack stack = new MyLinkedStack();
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        String[] polland = a.split(",");
        int result = 0 ;
        Elem help;
        for (int i = 0; i < polland.length; i++){
            if(check(polland[i])){
                stack.push(number(polland[i]));
            }else{

                switch (polland[i]){
                    case "*" :
                        help = (Elem)stack.pop();
                        result = (int)help.getValue();
                        help = (Elem)stack.pop();
                        result *= (int)help.getValue();
                        stack.push(result);
                        break;
                    case "/" :
                        help = (Elem)stack.pop();
                        result = (int)help.getValue();
                        help = (Elem)stack.pop();
                        result /= (int)help.getValue();
                        stack.push(result);
                        break;
                    case "-" :
                        help = (Elem)stack.pop();
                        result = (int)help.getValue();
                        help = (Elem)stack.pop();
                        result -= (int)help.getValue();
                        stack.push(result);
                        break;
                    case "+" :
                        help = (Elem)stack.pop();
                        result = (int)help.getValue();
                        help = (Elem)stack.pop();
                        result += (int)help.getValue();
                        stack.push(result);
                        break;
                }
            }
        }
        System.out.println(result);
    }
}
