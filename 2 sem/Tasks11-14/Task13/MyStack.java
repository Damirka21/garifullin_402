package Tasks11_14.Task13;

/**
 * Created by Damir on 10.03.2015.
 */
public interface MyStack<T> {
    public T push(T element);

    public T pop();

    public boolean isEmpty();

}
