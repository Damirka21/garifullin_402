package Tasks11_14.Task13;

/**
 * Created by Damir on 10.03.2015.
 */
public class MyLinkedStack<T> implements MyStack {
    Elem<T> head = null, p =   null;

    @Override
    public Object push(Object element) {
        p = new Elem<T>((T)element,head);
        head = p;
        return (T)element;
    }

    @Override
    public Object pop() {
        p = head;
        head = head.getNext();
        return p;
    }

    @Override
    public boolean isEmpty() {
        if(head != null){
            return false;
        }else {
            return true;
        }
    }
}
