package Tasks11_14.Task12;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Damir on 09.03.2015.
 */
public class MyLinkedList extends MyLinkedCollection implements List<Integer> {

    Elem<Integer> head = null, p = null;

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
    }

    @Override
    public Integer get(int index) {
        p = head;
        for (int i= 0; i < index; i ++){
            p = p.getNext();
        }
        return p.getValue();
    }   //complete

    @Override
    public Integer set(int index, Integer element) {
        p = head;
        for (int i= 0; i < index; i ++){
            p = p.getNext();
        }
        int set = p.getValue();
        p.setValue(element);
        return set;
    }   //complete

    @Override
    public void add(int index, Integer element) {
        p = head;
        for (int i= 0; i < index - 1; i ++){
            p = p.getNext();
        }
        Elem p1 = new Elem(element,p.getNext());
        p.setNext(p1);
    }   //complete

    @Override
    public Integer remove(int index) {
        p = head;
        for (int i= 0; i < index - 1; i ++){
            p = p.getNext();
        }
        int rem = (Integer)p.getNext().getValue();
        p.setNext(p.getNext().getNext());
        return rem;
    }   //complete

    @Override
    public int indexOf(Object o) {
        p = head;
        int i = 0;
        while (p.getValue() != o && p != null){
            p = p.getNext();
            i++;
        }
        if (i >= size()){
            return -1;
        }{
            return i;
        }
    }   //complete

    @Override
    public int lastIndexOf(Object o) {
        p = head;
        int last = -1;
        for (int i= 0; i < size(); i ++){
            if (p.getValue() == o){
                last = i;
            }
            p = p.getNext();
        }
        return last;
    }   //complete

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }   //complete

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }   //complete

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        p = head;
        MyLinkedList subList1 = new MyLinkedList();
        for (int i= 0; i < fromIndex; i ++){
            p = p.getNext();
        }
        for (int i= fromIndex; i < toIndex; i ++){
            subList1.add(p.getValue());
            p = p.getNext();
        }
        return subList1;
    }   //complete
}
