package Tasks11_14.Task12;

import Tasks11_14.Task11.MyCollection;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Damir on 09.03.2015.
 */
public class MyList<T> extends MyCollection<T> implements List<T> {

    Elem head = null, p = null;

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        for (Object o : c){
            add(index,(T)o);
            index ++;
        }
        return true;
    }   //complete

    @Override
    public T get(int index) {
        p = head;
        for (int i = 0; i < index; i++) {
            p = p.getNext();
        }
        return (T)p.getValue();
    }   //complete

    @Override
    public T set(int index, T element) {
        for (int i = 0; i < index ; i++) {
            p = p.getNext();
        }
        T a = (T)p.getValue();
        p.setValue(element);
        return a;
    }   //complete

    @Override
    public void add(int index, T element) {
        p = head;
        for (int i = 0; i < index; i++) {
            p = p.getNext();
        }
        Elem<T> p1=new Elem(element,p.getNext());
        p.setNext(p1);
    }   //complete

    @Override
    public T remove(int index) {
        for (int i = 0; i < index - 1; i++) {
            p = p.getNext();
        }
        T a = (T)p.getNext().getValue();
        p.setNext(p.getNext().getNext());
        return a;
    }   //complete

    @Override
    public int indexOf(Object o) {
        p = head;
        int i = 0;
        while (p.getValue() != o && p != null){
            p = p.getNext();
            i++;
        }
        if (i >= size()){
            return -1;
        }{
            return i;
        }
    }   //complete

    @Override
    public int lastIndexOf(Object o) {
        p = head;
        int last = -1;
        for (int i= 0; i < size(); i ++){
            if (p.getValue() == o){
                last = i;
            }
            p = p.getNext();
        }
        return last;
    }   //complete

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }   //complete

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }   //complete

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        p = head;
        MyCollection<T> subList1 = new MyCollection<T>();
        for (int i= 0; i < fromIndex; i ++){
            p = p.getNext();
        }
        for (int i= fromIndex; i < toIndex; i ++){
            subList1.add((T)p.getValue());
            p = p.getNext();
        }
        return (List)subList1;
    }   //complete
}
