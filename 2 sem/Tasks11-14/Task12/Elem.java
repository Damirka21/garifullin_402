package Tasks11_14.Task12;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Elem<T>{
    private T value;

    public Elem(T value, Elem next) {
        this.value = value;
        this.next = next;
    }

    private Elem next;

    public Elem() {


    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Elem getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }
}
