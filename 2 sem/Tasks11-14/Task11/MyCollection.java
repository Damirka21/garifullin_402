package Tasks11_14;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Damir on 09.03.2015.
 */
public class MyCollection<T> implements Collection<T> {

    Elem head = null, p = null;

    @Override

    public int size() {
        int size = 0;
        p = head;
        while (p != null) {
            size++;
            p = p.getNext();
        }
        return size;
    }   //complete

    @Override
    public boolean isEmpty() {
        if (head == null) {
            return true;
        } else {
            return false;
        }
    }   //complete

    @Override
    public boolean contains(Object o) {
        if (!isEmpty()) {
            boolean right = false;
            p = head;
            while (p != null) {
                if (p.getValue() == o) {
                    right = true;
                }
                p = p.getNext();
            }
            return right;
        } else {
            return false;
        }
    }   //complete

    @Override
    public Iterator<T> iterator() {
        return null;
    }   //complete

    @Override
    public Object[] toArray() {
        return new Object[0];
    }   //complete

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }   //complete

    @Override
    public boolean add(T t) {
        if (head != null) {
            p = head;

            while (p.getNext() != null) {
                p = p.getNext();
            }
            Elem p1 = new Elem(t, null);
            p.setNext(p1);
            return true;
        } else {
            p = new Elem(t, head);
            head = p;
            return true;
        }
    }   //complete

    @Override
    public boolean remove(Object o) {
        if (!isEmpty() && head.getNext() != null) {
            p = head;
            while (p.getNext().getValue() != o) {
                p = p.getNext();
            }
            p.setNext(p.getNext().getNext());
            return true;
        } else {
            return false;
        }
    }   //complete

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }   //complete

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean cont = false;
        for (Object o : c) {
            cont = add((T) o);
        }
        return cont;
    }   //complete

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean rem = false;
        for (Object o : c) {
            rem = remove(o);
        }
        return rem;
    }   //complete

    @Override
    public boolean retainAll(Collection<?> c) {
        Elem head1 = null, p1;
        for (Object o : c) {
            if (contains(o)) {
                p1 = new Elem(o, head1);
                head1 = p1;
            }
        }
        head = head1;
        p = head1;
        if (head != null) {
            return true;
        } else {
            return false;
        }
    }   //complete

    @Override
    public void clear() {
        head = null;
        p = null;
    } // complete
}
