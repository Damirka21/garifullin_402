package Tasks11_14;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by Damir on 06.03.2015.
 */
public class MyArrayCollection implements Collection<Integer> {

    static int[] arr = new int[1000000];

    @Override
    public int size() {
        int size = 0;
        while (arr[size] != 0) {
            size++;
        }
        return size;
    }   //complete

    @Override
    public boolean isEmpty() {
        if (arr[0] == 0) {
            return true;
        } else {
            return false;
        }

    }   //complete

    @Override
    public boolean contains(Object o) {
        if (!isEmpty()) {
            boolean cont = false;
            int i = 0;
            while (arr[i] != 0) {
                if (arr[i] == o) {
                    cont = true;
                }
            }
            return cont;
        } else {
            return false;
        }
    }   //complete

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }   //complete

    @Override
    public Object[] toArray() {
        return new Object[0];
    }   //complete

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }   //complete

    @Override
    public boolean add(Integer integer) {
        int i = 0;
        while (arr[i] != 0) {
            i++;
        }
        arr[i] = integer;
        return true;
    }   //complete

    @Override
    public boolean remove(Object o) {
        int i = 0;
        while (arr[i] != o) {
            i++;
        }
        while (arr[i] != 0) {
            arr[i] = arr[i + 1];
            i++;
        }
        return true;
    }   //complete

    @Override
    public boolean containsAll(Collection<?> c) {
        if (!isEmpty()) {
            int i = 0;
            boolean cont = true;
            for (Object o : c) {
                if (!this.contains(o)) {
                    return false;
                }
            }
            return cont;
        } else {
            return false;
        }
    }   //complete

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        for (Object o : c) {
            add((Integer) o);
        }
        return true;
    }   //complete

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean rem = true;
        for (Object o : c) {
            if (!this.contains(o)) {
                remove(o);
            } else {
                return false;
            }
        }
        return rem;
    }   //complete

    @Override
    public boolean retainAll(Collection<?> c) {
        int i = 0;
        for (Object o : c) {
            if (this.contains(o)) {
                arr[i] = (Integer) o;
                i++;
            }
        }
        while (arr[i] != 0) {
            arr[i] = 0;
            i++;
        }
        return true;
    } //complete

    @Override
    public void clear() {
        int i = 0;
        while (arr[i] != 0) {
            arr[i] = 0;
        }
    }   //complete
}
