package Tasks11_14;

import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Damir on 09.03.2015.
 */
public class MyArrayList extends MyArrayCollection implements List<Integer> {

    int[] array = new int[10000000];

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        for (Object o : c) {
            add(index, (Integer) o);
            index++;
        }
        return true;
    }   //complete

    @Override
    public Integer get(int index) {
        return array[index];
    }   //complete

    @Override
    public Integer set(int index, Integer element) {
        int a = array[index];
        array[index] = element;
        return a;
    }   //complete

    @Override
    public void add(int index, Integer element) {
        int i = size();
        while (i > index) {
            array[i] = array[i - 1];
            i--;
        }
        arr[index] = element;
    }   //complete

    @Override
    public Integer remove(int index) {
        int a = array[index];
        while (array[index] != 0) {
            array[index] = array[index + 1];
        }
        return a;
    }   //complete

    @Override
    public int indexOf(Object o) {
        int i = 0;
        while (array[i] != 0) {
            if (array[i] == (Integer) o) {
                return i;
            } else {
                i++;
            }
        }
        return -1;
    }   //complete

    @Override
    public int lastIndexOf(Object o) {
        int a = -1, i = 0;
        while (array[i] != 0) {
            if (array[i] == (Integer) o) {
                a = i;
            }
            i++;
        }
        return a;
    }   //complete

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }   //complete

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }   //complete

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        MyArrayList subList = new MyArrayList();
        int i = fromIndex;
        while (i < toIndex) {
            subList.add(array[i]);
            i++;
        }

        return subList;

    }   //complete
}
