/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task03c {
    static int x = 2;

    public static void table(int a) {
        if (x < 10) {
            System.out.println(a + " x " + x + " = " + (a * x));
            x++;
            table(a);
        }
    }


    public static int factorial(int a) {
        if (a == 0){
            return 1;
        }else {
            return a * factorial(a - 1);
        }
    }

    public static double Vallis(int a) {
        if (a > 0) {
            return   2 * a / (2 * a - 1) * 2 * a / (2 * a + 1)* Vallis(a - 1);

        } else{
            return 1;
        }

    }

    public static void main(String[] args) {
        int fac = 1;
        table(fac);
        int c = factorial(fac);
        System.out.println(fac + "! = " + c);
        double v = Vallis(fac);
        System.out.println("Vallis for " + fac + " = " + v);
    }
}
