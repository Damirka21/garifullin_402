package Task20_22;

/**
 * Created by Damir on 06.04.2015.
 */
public class SuperBooperTree<T> {

    public SuperBooperNode getRoot() {
        return root;
    }

    public SuperBooperTree(SuperBooperNode root) {
        this.root = root;
    }

    public void setRoot(SuperBooperNode root) {
        this.root = root;
    }

    private SuperBooperNode root;
}
