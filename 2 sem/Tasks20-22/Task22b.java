package Task20_22;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Damir on 20.04.2015.
 */
public class Task22b {

    public static void main(String[] args) throws FileNotFoundException {
        SuperBooperNode root = new SuperBooperNode("0");
        SuperBooperTree tree = new SuperBooperTree(root);
        SuperBooperNode node1L = new SuperBooperNode("10");
        SuperBooperNode node1R = new SuperBooperNode("1");
        SuperBooperNode node1RL = new SuperBooperNode("11");
        root.setRight(node1R);
        root.setLeft(node1L);
        node1R.setLeft(node1RL);

        Scanner scanner = new Scanner(new File("in.txt"));
        String s = scanner.next();
        SuperBooperNode a = root;
        int i = 0;
        while(!s.isEmpty()){
            if(s.charAt(i) == (char)a.getValue()){
                a = a.getLeft();
            }else{
                System.out.println("ERROR");
                break;
            }
            if(s.charAt(i) == (char)a.getValue()){
                a = root;
            }else{
                System.out.println("ERROR");
                break;
            }
        }

    }
}
