package Task20_22;

import java.util.*;

/**
 * Created by Damir on 20.04.2015.
 */
public class MyTreeSet1<T extends Comparable<T>> implements Set<T> {
    private SuperBooperNode<T> root;

    int i ;
    @Override
    public int size() {
        SuperBooperNode node = root;
        Stack<SuperBooperNode> stack = new Stack<SuperBooperNode>();
        stack.push(node);
        int n = 0;
        while (!stack.isEmpty()) {
            node = stack.pop();
            if (!node.getColor()) {
                node.setColor(true);
                stack.push(node);
                if (node.getRight() != null) {
                    stack.push(node.getRight());
                }
                if (node.getLeft() != null) {
                    stack.push(node.getLeft());
                }
            } else {
                n++;

            }
        }
        return n;
    } //complete

    @Override
    public boolean isEmpty() {

        return root == null;
    } //complete

    @Override
    public boolean contains(Object o) {
        SuperBooperNode<T> node = root;
        while (node != null) {
            if (o.equals(node.getValue())) {
                return true;
            }

            if (node.getValue().compareTo((T) o) < 0) {
                node = node.getLeft();
            } else {
                node = node.getRight();
            }
        }
        return false;
    } //complete

    @Override
    public Iterator<T> iterator() {

        i = 0;
        Iterator<T> iterator = new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return i < size() - 1;
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                } else {
                    return (T) toArray()[i++];
                }
            }
        };
        return null;
    } //complete


    @Override
    public Object[] toArray() {
        Object[] result = new Object[size()];
        SuperBooperNode<T> node = root;
        Stack<SuperBooperNode<T>> stack = new Stack<SuperBooperNode<T>>();
        stack.push(node);
        int n = 0;
        while (!stack.isEmpty()) {
            node = stack.pop();
            if (!node.getColor()) {
                node.setColor(true);
                stack.push(node);
                if (node.getRight() != null) {
                    stack.push(node.getRight());
                }
                if (node.getLeft() != null) {
                    stack.push(node.getLeft());
                }
            } else {
                result[n] = node.getValue();
                n++;

            }
        }
        return result;
    }   //complete

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        SuperBooperNode<T> node = root;
        while (node != null) {
            if (t.equals(node.getValue())) {
                return true;
            }

            if (t.compareTo(node.getValue()) < 0) {
                node = node.getLeft();
            } else {
                node = node.getRight();
            }
        }
        node.setValue(t);

        return true;
    }   //complete

    @Override
    public boolean remove(Object o) {
        if (contains(o)) {
            SuperBooperNode<T> neew = new SuperBooperNode<T>((T) o);
            SuperBooperNode<T> node = root;
            SuperBooperNode<T> zamena = null;
            SuperBooperNode<T> parent = null;
            while (node != null) {
                if (neew.getValue().equals(node.getValue())) {
                    zamena = node;
                    break;
                }
                if (neew.getValue().compareTo(node.getValue()) < 0) {
                    node = node.getLeft();
                } else {
                    node = node.getRight();
                }
            }
            node = node.getLeft();
            while (node.getRight() != null) {
                parent = node;
                node = node.getRight();
            }
            if (node.getLeft() != null) {
                parent.setRight(node.getLeft());
            }
            zamena.setValue(node.getValue());
            node = null;
            return true;
        } else {
            return false;
        }
    } //complete

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object a : c) {
            if (!contains(a)) {
                return false;
            }
        }
        return true;
    } //complete

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T a : c) {
            add(a);
        }
        return true;
    } //complete

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean k = true;
        for (T t : this){
            if(!c.contains(t)){
                remove(t);
                k = false;
            }
        }
        return k;
    }   //complete

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object a : c) {
            if (contains(a)) {
                remove(a);
            }
        }
        return true;
    }   //complete

    @Override
    public void clear() {
        root = null;
    } //complete
}
