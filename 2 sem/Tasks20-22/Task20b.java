package Task20_22;

import java.util.Stack;

/**
 * Created by Damir on 18.04.2015.
 */
public class Task20b {
    public static void travel(SuperBooperTree tree) {
        SuperBooperNode node = tree.getRoot();
        Stack<SuperBooperNode> stack = new Stack<SuperBooperNode>();
        while (node!= null || !stack.isEmpty()) {
            if (!stack.isEmpty()) {
                node = stack.pop();
                System.out.println(node.getValue() + "traveled");
                if (node.getRight() != null) {
                    node = node.getRight();
                }else {
                    node = null;
                }
            }
            while (node != null){
                stack.push(node);
                node = node.getLeft();
            }
        }
    }


    public static void main(String[] args) {
        SuperBooperNode root = new SuperBooperNode(5);
        SuperBooperTree tree = new SuperBooperTree(root);
        SuperBooperNode node1L = new SuperBooperNode(3);
        SuperBooperNode node1R = new SuperBooperNode(1);
        SuperBooperNode node1LL = new SuperBooperNode(1);
        SuperBooperNode node1LR = new SuperBooperNode(9);
        SuperBooperNode node1RL = new SuperBooperNode(6);
        node1L.setLeft(node1LL);
        node1L.setRight(node1LR);
        node1R.setLeft(node1RL);
        root.setLeft(node1L);
        root.setRight(node1R);

        travel(tree);
    }
}
