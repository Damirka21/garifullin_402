package Task20_22;

import java.util.*;

/**
 * Created by Damir on 18.04.2015.
 */
public class MyTreeSet implements Set<Integer> {

    private SuperBooperNode<Integer> root;

    int i;

    @Override
    public int size() {
        SuperBooperNode node = root;
        Stack<SuperBooperNode> stack = new Stack<SuperBooperNode>();
        stack.push(node);
        int n = 0;
        while (!stack.isEmpty()) {
            node = stack.pop();
            if (!node.getColor()) {
                node.setColor(true);
                stack.push(node);
                if (node.getRight() != null) {
                    stack.push(node.getRight());
                }
                if (node.getLeft() != null) {
                    stack.push(node.getLeft());
                }
            } else {
                n++;

            }
        }
        return n;
    }//complete

    @Override
    public boolean isEmpty() {
        return root == null;
    }//complete

    @Override
    public boolean contains(Object o) {
        SuperBooperNode<Integer> node = root;
        while (node != null) {
            if (o == node.getValue()) {
                return true;
            }

            if (node.getValue() > (Integer) o) {
                node = node.getLeft();
            } else {
                node = node.getRight();
            }
        }
        return false;
    }//complete

    @Override
    public Iterator<Integer> iterator() {
        i = 0;
        Iterator<Integer> iterator = new Iterator<Integer>() {
            @Override
            public boolean hasNext() {
                return i < size() - 1;
            }

            @Override
            public Integer next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                } else {
                    return (Integer) toArray()[i++];
                }
            }
        };
        return null;
    } //complete
    @Override
    public Object[] toArray() {
        Object[] result = new Object[size()];
        SuperBooperNode node = root;
        Stack<SuperBooperNode> stack = new Stack<SuperBooperNode>();
        stack.push(node);
        int n = 0;
        while (!stack.isEmpty()) {
            node = stack.pop();
            if (!node.getColor()) {
                node.setColor(true);
                stack.push(node);
                if (node.getRight() != null) {
                    stack.push(node.getRight());
                }
                if (node.getLeft() != null) {
                    stack.push(node.getLeft());
                }
            } else {
                result[n] = node.getValue();
                n++;

            }
        }
        return result;
    } // complete

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        SuperBooperNode<Integer> node = root;
        while (node != null) {
            if (integer == node.getValue()) {
                return true;
            }

            if (node.getValue() > integer) {
                node = node.getLeft();
            } else {
                node = node.getRight();
            }
        }
        node.setValue(integer);

        return true;
    } //complete

    @Override
    public boolean remove(Object o) {
        if (contains(o)) {
            SuperBooperNode<Integer> node = root;
            SuperBooperNode<Integer> zamena = null;
            SuperBooperNode<Integer> parent = null;
            while (node != null) {
                if (o == node.getValue()) {
                    zamena = node;
                    break;
                }
                if (node.getValue() > (Integer) o) {
                    node = node.getLeft();
                } else {
                    node = node.getRight();
                }
            }
            node = node.getLeft();
            while (node.getRight() != null){
                parent = node;
                node = node.getRight();
            }
            if(node.getLeft() != null){
                parent.setRight(node.getLeft());
            }
            zamena.setValue(node.getValue());
            node = null;
            return true;
        } else {
            return false;
        }
    } //complete

    @Override
    public boolean containsAll(Collection<?> c) {
        for(Object a : c){
            if(!contains(a)){
                return false;
            }
        }
        return true;
    }   //complete

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        for(Object a : c){
            add((Integer)a);
        }
        return true;
    } //complete

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean k = true;
        for (Integer t : this){
            if(!c.contains(t)){
                remove(t);
                k = false;
            }
        }
        return k;
    } //complete

    @Override
    public boolean removeAll(Collection<?> c) {
        for(Object a : c){
            if(contains(a)){
                remove(a);
            }
        }
        return true;
    }   //complete

    @Override
    public void clear() {
        root = null;
    } //complete
}
