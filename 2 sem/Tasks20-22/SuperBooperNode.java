package Task20_22;

/**
 * Created by Damir on 06.04.2015.
 */
public class SuperBooperNode<T> {

    public SuperBooperNode(T value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    private T value;

    public boolean getColor() {
        return color;
    }

    public void setColor(boolean color) {
        this.color = color;
    }

    private boolean color = false;


    public SuperBooperNode getLeft() {
        return left;
    }

    public void setLeft(SuperBooperNode left) {
        this.left = left;
    }

    public SuperBooperNode getRight() {
        return right;
    }

    public void setRight(SuperBooperNode right) {
        this.right = right;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    private SuperBooperNode left,right;
}
