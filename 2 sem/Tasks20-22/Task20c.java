package Task20_22;

import Task18_19.IntegerComparator;

import java.util.Stack;

/**
 * Created by Damir on 18.04.2015.
 */
public class Task20c {

    public static int sum(SuperBooperNode<Integer> node, int a) {
        a += node.getValue();
        if (node.getLeft() != null) {
            a = sum(node.getLeft(), a);
        }
        if (node.getRight() != null) {
            a = sum(node.getRight(), a);
        }

        return a;
    }

    public static int mult(SuperBooperNode<Integer> node, int a) {

        if (node.getLeft() != null) {
            a = mult(node.getLeft(), a);
        }
        if (node.getRight() != null) {
            a = mult(node.getRight(), a);
        }
        a *= node.getValue();
        return a;
    }

    public static int travel(SuperBooperNode<Integer> node) {
        Stack<SuperBooperNode<Integer>> stack = new Stack<SuperBooperNode<Integer>>();
        stack.push(node.getRight());
        stack.push(node.getLeft());
        stack.push(node);
        while (!stack.isEmpty()) {
            SuperBooperNode node1 = stack.pop();
            if (node1.getRight() != null) {
                stack.push(node1.getRight());
            }
            if (node1.getLeft() != null) {
                stack.push(node1.getLeft());
            }
        }
        int max = stack.pop().getValue();
        while (!stack.isEmpty()) {
            int a = stack.pop().getValue();
            if (max < a){
                max = a;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        SuperBooperNode root = new SuperBooperNode(5);
        SuperBooperTree tree = new SuperBooperTree(root);
        SuperBooperNode node1L = new SuperBooperNode(3);
        SuperBooperNode node1R = new SuperBooperNode(1);
        SuperBooperNode node1LL = new SuperBooperNode(1);
        SuperBooperNode node1LR = new SuperBooperNode(4);
        SuperBooperNode node1RL = new SuperBooperNode(6);
        node1L.setLeft(node1LL);
        node1L.setRight(node1LR);
        node1R.setLeft(node1RL);
        root.setLeft(node1L);
        root.setRight(node1R);

        System.out.println("sum = " + sum(root, 0));
        System.out.println("mult = " + mult(root, 1));
        System.out.println("max = " + travel(root));
    }
}
