package Task20_22;

import java.util.Stack;

/**
 * Created by Damir on 18.04.2015.
 */
public class Task20a {

    public static void travel(SuperBooperTree tree) {
        SuperBooperNode node = tree.getRoot();
        Stack<SuperBooperNode> stack = new Stack<SuperBooperNode>();
        stack.push(node);
        int n = 0;
        while (!stack.isEmpty()) {
            node = stack.pop();
            if(!node.getColor()){
                node.setColor(true);
                stack.push(node);
                if (node.getRight() != null){
                    stack.push(node.getRight());
                }
                if(node.getLeft() != null){
                    stack.push(node.getLeft());
                }
            }else{
                node.setValue(n);
                n++;

            }
        }
    }


    public static void main(String[] args) {
        SuperBooperNode root = new SuperBooperNode(5);
        SuperBooperTree tree = new SuperBooperTree(root);
        SuperBooperNode node1L = new SuperBooperNode(11);
        SuperBooperNode node1R = new SuperBooperNode(1);
        SuperBooperNode node1LL = new SuperBooperNode(1);
        SuperBooperNode node1LR = new SuperBooperNode(9);
        SuperBooperNode node1RL = new SuperBooperNode(6);
        node1L.setLeft(node1LL);
        node1L.setRight(node1LR);
        node1R.setLeft(node1RL);
        root.setLeft(node1L);
        root.setRight(node1R);
        travel(tree);
    }
}
