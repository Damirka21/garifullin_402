package Task18_19;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Created by Damir on 23.03.2015.
 */
public class Task19b {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("in.txt"));
        ArrayList<Integer> list = new ArrayList<Integer>();
        while(scanner.hasNext()){
            list.add(scanner.nextInt());
        }
        Collections.sort(list,new IntegerComparator());
        FileWriter file = new FileWriter(new File("out.txt"));
        System.out.println(list);
        Iterator iterator =  list.listIterator();
        while(iterator.hasNext()){
            file.write(iterator.next() + " ");
        }
        file.close();
    }
}
