package Task18_19;

import Task18_19.Vector2D;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Damir on 23.03.2015.
 */
public class Task18c {
    public static void main(String[] args) {
        ArrayList<Vector2D> list = new ArrayList<Vector2D>();
        Vector2D vec1 = new Vector2D(2,3);
        Vector2D vec3 = new Vector2D(0,0);
        Vector2D vec2 = new Vector2D(1,1);
        Vector2D vec4 = new Vector2D(2,9);
        Vector2D vec5 = new Vector2D(5,5);
        list.add(vec1);
        list.add(vec2);
        list.add(vec3);
        list.add(vec4);
        list.add(vec5);
        Collections.sort(list);
        System.out.println(list);
    }
}
