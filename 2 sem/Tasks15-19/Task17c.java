package Task18_19;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Damir on 22.03.2015.
 */
public class Task17c {
    public static void main(String[] args) {
        HashMap<Character, Integer> example = new HashMap<Character, Integer>();
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        for (int i = 0; i < line.length(); i++){
            if(example.get(line.charAt(i))== null){
                example.put(line.charAt(i),1);
            }else{
                example.put(line.charAt(i),example.get(line.charAt(i))+1);
            }
        }
        System.out.println(example.toString());
    }
}