package Task18_19;

import java.util.Comparator;

/**
 * Created by Damir on 23.03.2015.
 */
public class IntegerComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer o1, Integer o2) {
        int r1 = o1,r2 = o2;
        while (r1 != 0 && r2 != 0) {
            r1 /= 10;
            r2 /= 10;
        }
        if (r1 == 0 && r2 == 0){
            if (o1 > o2){
                return 1;
            }else{
                return -1;
            }
        }else{
            if (r1 == 0){
                return -1;
            }else{
                return 1;
            }
        }
    }
}
