package Task18_19;

import java.util.Comparator;

/**
 * Created by Damir on 23.03.2015.
 */
public class ListComparator implements Comparator<Vector2D> {
    @Override
    public int compare(Vector2D o1, Vector2D o2) {
        if (o1.getX() > o2.getX()){
            return 1;
        }else{
            if(o1.getX() == o2.getX()){
                if (o1.getY() > o2.getY()){
                    return 1;
                }else{
                    return -1;
                }
            }
            return -1;
        }
    }
}
