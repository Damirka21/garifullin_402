/**
 * @author Damir Garifullin
 *         11-402
 */


public class Task02c {

    static int deg = 1;

    public static void degree(int a, int b) {
        if (b > 0) {
            deg *= a;
            b--;
            degree(a, b);
        } else {
            deg *= 1;
            System.out.println(deg);
        }
    }

    public static void main(String[] args) {
        degree(2, 0);
    }
}
