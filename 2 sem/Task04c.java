import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task04c {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }
        boolean us = false;
        while (p != null) {
            if (p.getValue() == 0) {
                us = true;
            }
            p = p.getNext();
        }
        System.out.println(us);
    }
}
