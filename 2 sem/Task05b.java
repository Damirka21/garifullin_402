import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task05b {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }
        int s = 1, mult = 1;
        while (p != null) {
            if (s % 2 == 1) {
                mult *= p.getValue();
            }
            p = p.getNext();
            s++;
        }
        System.out.println(mult);
    }
}
