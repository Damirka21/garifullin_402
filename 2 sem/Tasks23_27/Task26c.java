package Tasks23_27;

import java.io.*;

/**
 * Created by Damir on 09.05.2015.
 */
public class Task26c {

    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader(new File("in.txt"));
        FileWriter fw = new FileWriter(new File("out.txt"));
        long t1= System.nanoTime();
        while(fr.ready()){
            fw.write(fr.read());
        }
        t1 = (t1 - System.nanoTime()) * (-1);
        fw.close();
        fr.close();
        BufferedReader br = new BufferedReader(new FileReader(new File("in.txt")));
        PrintWriter pw = new PrintWriter(new File("out.txt"));
        long t2 = System.nanoTime();
        while(br.ready()){
            pw.print(br.readLine());
        }
        t2 = (t2 - System.nanoTime()) * (-1);
        br.close();
        pw.close();
        System.out.println("Symbol : " + t1);
        System.out.println("Line : " + t2);
    }
}
