package Tasks23_27;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Damir on 10.05.2015.
 */
public class MyThread implements Runnable {

    private Thread thread;

    private String name;
    private int a;

    public void join() throws InterruptedException {
        thread.join();
    }

    public int getSum() {
        return sum;
    }

    private int sum = 0;

    public MyThread(String name, int a) {
        this.name = name;
        this.a = a;
    }


    @Override
    public void run() {
        for (int i = 0; i < 50000000; i++)  {
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            int newTime;
            if ((calendar.get(Calendar.HOUR_OF_DAY) + a < 0)) {
                newTime = (calendar.get(Calendar.HOUR_OF_DAY)  + a) + 24;
            } else {
                if ((calendar.get(Calendar.HOUR_OF_DAY)  + a > 24)) {
                    newTime = (calendar.get(Calendar.HOUR_OF_DAY)  + a) - 24;
                } else {
                    newTime = calendar.get(Calendar.HOUR_OF_DAY)  + a;
                }

            }
            System.out.println(name + " " + newTime + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
