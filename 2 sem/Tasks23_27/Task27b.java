package Tasks23_27;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by Damir on 09.05.2015.
 */
public class Task27b {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number of :");
        int n = scanner.nextInt();
        scanner.nextLine();
        ArrayList<Thread> arr = new ArrayList<Thread>();
        for(int i = 0; i < n; i ++) {
            String s = scanner.nextLine();
            String b = scanner.nextLine();
            String c = "";
            int a ;
            if(b.charAt(0) != '0') {
                if (b.charAt(0) == '+') {
                    for (int j = 1; j < b.length(); j++) {
                        c += b.charAt(j);
                    }
                    a = Integer.parseInt(c);
                } else {
                    for (int j = 1; j < b.length(); j++) {
                        c += b.charAt(j);
                    }
                    a = Integer.parseInt(c);
                    a *= -1;
                }
            }else{
                a = 0;
            }
            Thread t1 = new Thread(new MyThread(s,a));
            arr.add(t1);
        }
        for (Thread c : arr){
            c.start();
        }

    }
}

