package Tasks23_27;

import java.util.ArrayList;

/**
 * Created by Damir on 03.05.2015.
 */
public class SuperNode {
    public int getValue() {
        return value;
    }

    public ArrayList<Integer> getArr() {
        return arr;
    }

    public ArrayList<Integer> arr;

    public void setValue(int value) {
        this.value = value;
    }

    private int value;

    public SuperNode(int value) {
        this.value = value;
    }
}
