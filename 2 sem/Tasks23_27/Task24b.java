package Tasks23_27;

import java.util.Scanner;

/**
 * Created by Damir on 09.05.2015.
 */
public class Task24b {

    static int m,k;
    public static void numbers(int count, int literals,String string) {
        if (literals < m) {
            if (count <= k) {
                for (int i = 0; i < 10 ; i++ ) {
                    numbers(count + i, literals + 1,string + i);
                }
            }
        } else {
            if (count <= k) {
                System.out.println(string);
            }
        }

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        k = scanner.nextInt();
        m = scanner.nextInt();
        numbers(0,0,"");
    }
}
