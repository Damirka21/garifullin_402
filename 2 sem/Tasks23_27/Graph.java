package Tasks23_27;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Damir on 24.02.2015.
 */
public class Graph {
    public Graph(ArrayList<Edge> edgesList) {
        EdgesList = edgesList;
        numbers();
        for (int i = 0; i < length; i++) {
            AdjecencyLists.add(new SuperNode(i));
        }
    }

    public Graph(int[][] MS) {
        this.MS = MS;
    }

    static Scanner scanner = new Scanner(System.in);
    public int length, countEdges;
    private ArrayList<Edge> EdgesList = getEdgesList();
    private ArrayList<SuperNode> AdjecencyLists = getAdjecencyLists();
    public int[][] MS = getAdjecencyMatrix();
    public int[][] MI = getIncidenceMatrix();

    public void numbers() {
        for (Edge p : EdgesList) {
            if (p.getSn1().getValue() > length) {
                length = p.getSn1().getValue();
            }
            if (p.getSn2().getValue() > length) {
                length = p.getSn2().getValue();
            }
            countEdges++;
        }
        length++;// 0 - тоже вершина
    }   //additional

    public ArrayList<Edge> getEdgesList() {
        int[][] mi = getIncidenceMatrix();
        ArrayList<Edge> arr = null;
        ArrayList<SuperNode> nodes = new ArrayList<SuperNode>();
        for (int i = 0; i < length; i++) {
            nodes.add(new SuperNode(i));
        }
        for (int j = 0; j < countEdges; j++) {
            Edge p = null;
            for (int i = 0; i < length; i++) {
                if (mi[i][j] == 1 && p.getSn1() == null) {
                    p.setSn1(nodes.get(i));
                } else {
                    if (mi[i][j] == 1) {
                        p.setSn2(nodes.get(i));

                    }
                }
                if (mi[i][j] == 2) {
                    p.setSn1(nodes.get(i));
                    p.setSn2(nodes.get(i));
                }
            }
            arr.add(p);
        }
        return arr;
    }//complete

    public int[][] getIncidenceMatrix() {
        numbers();
        int[][] mi = new int[length][countEdges];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < countEdges; j++) {
                mi[i][j] = 0;
            }
        }
        int i = 0;
        for (Edge p : EdgesList) {
            mi[p.getSn1().getValue()][i] += 1;
            mi[p.getSn2().getValue()][i] += 1;
            i++;
        }
        length++;
        return mi;
    }   //complete

    public ArrayList<SuperNode> getAdjecencyLists() {
        int[][] ms = getAdjecencyMatrix();
        for (int i = 0; i < length; i++) {
            for (int j = i; j < length; j++) {
                if (ms[i][j] == 1) {
                    AdjecencyLists.get(i).getArr().add(j);
                    AdjecencyLists.get(j).getArr().add(i);
                }
            }
        }
        return null;
    }   //complete

    public ArrayList<SuperNode> getAdjecencyListsFromMI() {
        int[][] mi = getIncidenceMatrix();
        ArrayList<SuperNode> arr = null;
        int sn1 = 0, sn2 = 0, check = 0;
        for (int i = 0; i < countEdges; i++) {
            for (int j = 0; j < length; j++) {
                if (mi[j][i] == 1 && check == 0) {
                    sn1 = j;
                    check++;
                } else {
                    if (mi[j][i] == 1 && check == 1) {
                        sn2 = j;
                        check++;
                    }
                }
            }
            arr.get(sn1).getArr().add(sn2);
            arr.get(sn2).getArr().add(sn1);
            check = 0;
        }
        return arr;
    }   //complete

    private int[][] getAdjecencyMatrix() {
        numbers();
        int[][] mi = new int[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                mi[i][j] = 0;
            }
        }
        for (Edge p : EdgesList) {
            mi[p.getSn1().getValue()][p.getSn2().getValue()] = 1;
            mi[p.getSn2().getValue()][p.getSn1().getValue()] = 1;
        }
        return mi;
    } //complete

    public int[][] getMIfromMS() {
        int[][] mii = getAdjecencyMatrix();
        int[][] mi = new int[length][countEdges];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < countEdges; j++) {
                mi[i][j] = 0;
            }
        }
        int k = 0;
        for (int i = 0; i < length; i++) {
            for (int j = i; j < length; j++) {
                if (mii[i][j] == 1) {
                    mi[k][i] = 1;
                    mi[k][j] = 1;
                    k++;
                }
            }
        }
        return mi;
    }   //complete

    public int[][] getMSfromMI() {
        int[][] mii = getIncidenceMatrix();
        int[][] mi = new int[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                mi[i][j] = 0;
            }
        }
        int k = 0;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < countEdges; j++) {
                if (mii[i][j] == 1) {
                    if (k == 0) {
                        k = j;
                    } else {
                        mi[k][j] = 1;
                        mi[j][k] = 1;
                        k = 0;
                    }
                }
            }
        }
        return mi;
    }   //complete

    public ArrayList<Edge> getEdgesListFromMS() {
        int[][] mi = getAdjecencyMatrix();
        ArrayList<Edge> arr = null;
        ArrayList<SuperNode> nodes = new ArrayList<SuperNode>();
        for (int i = 0; i < length; i++) {
            nodes.add(new SuperNode(i));
        }
        for (int i = 0; i < length; i++) {
            for (int j = i; j < length; j++) {
                if (mi[i][j] == 1) {
                    Edge example = new Edge(nodes.get(i), nodes.get(j));
                    arr.add(example);
                }
            }
        }
        return arr;
    }

    public int[][] getAdjecencyMatrixFromAdjecencyList() {
        ArrayList<SuperNode> arr = getAdjecencyLists();
        int[][] ms = new int[length][length];
        for (SuperNode p : AdjecencyLists) {
            for (int a : p.getArr()) {
                ms[p.getValue()][a] = 1;
            }
        }
        return ms;
    } //complete

    public ArrayList<SuperNode> getAdjecencyListsFromEdgesList() {
        ArrayList<Edge> arr = getEdgesList();
        ArrayList<SuperNode> nodes = new ArrayList<SuperNode>();
        for (int i = 0; i < length; i++) {
            nodes.add(new SuperNode(i));
        }
        for (Edge p : EdgesList) {
            nodes.get(p.getSn1().getValue()).getArr().add(p.getSn2().getValue());
            nodes.get(p.getSn2().getValue()).getArr().add(p.getSn1().getValue());
        }
        return nodes;
    }   //complete

    public ArrayList<Edge> getEdgesListFromAdjecencyList() {
        ArrayList<SuperNode> nodes = getAdjecencyLists();
        ArrayList<Edge> edges = null;
        boolean check = false;
        for (SuperNode c : nodes) {
            for (int a : c.getArr()) {
                Edge edge = new Edge(c, nodes.get(a));
                for (Edge b : edges) {
                    if (edge.equals(b)) {
                        check = true;
                    }
                }
                if (!check) {
                    edges.add(edge);
                }
            }
        }
        return edges;
    }   //complete

    public int[][] getMIfromAdjecencyList() {
        int[][] mi = new int[length][countEdges];
        ArrayList<SuperNode> nodes = getAdjecencyLists();
        ArrayList<Edge> edges = null;
        int i = 0;
        boolean check = false;
        for (SuperNode c : nodes) {
            for (int a : c.getArr()) {
                Edge edge = new Edge(c, nodes.get(a));
                for (Edge b : edges) {
                    if (edge.equals(b)) {
                        check = true;
                    }
                }
                if (!check) {
                    mi[c.getValue()][i] = 1;
                    mi[a][i] = 1;
                    i++;
                }
            }
        }
        return mi;
    }   //complete

}