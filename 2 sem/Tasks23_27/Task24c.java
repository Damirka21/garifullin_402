package Tasks23_27;

import java.util.Scanner;

/**
 * Created by Damir on 08.05.2015.
 */
public class Task24c {

    public static int check(char a) {
        switch (a) {
            case 'a':
                return 1;
            case 'e':
                return 1;
            case 'i':
                return 1;
            case 'u':
                return 1;
            case 'o':
                return 1;
            case 'y':
                return 1;
            default:
                return 0;
        }
    }

    static int number;

    public static void words(int count, int literals, String string) {
        if (literals < number) {
            if (count <= 3) {
                for (char i = 'a'; i <= 'z'; i++) {
                    words(count + check(i), literals + 1, string + i);
                }
            }
        } else {
            if (count <= 3) {
                System.out.println(string);
            }
        }

    }

    public static void main(String[] args) {
        String string = "";
        Scanner scanner = new Scanner(System.in);
        number = scanner.nextInt();
        words(0, 0, string);
    }
}
