package Tasks23_27;

/**
 * Created by Damir on 03.05.2015.
 */
public class Edge {
    public Edge(SuperNode a1,SuperNode a2) {
        setSn1(a1);
        setSn2(a2);
    }


    public static SuperNode getSn1() {
        return sn1;
    }

    public static void setSn1(SuperNode sn1) {
        Edge.sn1 = sn1;
    }

    public boolean equals(Edge e){
        if (this.getSn1().getValue() == e.getSn1().getValue() && this.getSn2().getValue() == e.getSn2().getValue() ||
                this.getSn1().getValue() == e.getSn2().getValue() && this.getSn2().getValue() == e.getSn1().getValue()){
            return true;
        }else {
            return false;
        }
    }

    public static SuperNode getSn2() {
        return sn2;
    }

    public static void setSn2(SuperNode sn2) {
        Edge.sn2 = sn2;
    }

    private static SuperNode sn1= null, sn2;
}
