import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task03a {

    static boolean flag = true;
    static Scanner scn = new Scanner(System.in);
    static int n = scn.nextInt();
    static int[] a = new int[n];

    public static void scan(int i){
        if(flag){
            a[i] = scn.nextInt();
            if(i == n - 1){
                flag = false;
            }
            scan(i+1);
        }
    }

    public static void srt(int i){
        if(i < n - 1){
            sort(i, i+1);
            srt(i+1);
        }
    }

    public static void sort(int x, int y){

        if(a[x] > a[y]){
            a[x] += a[y];
            a[y] = a[x] - a[y];
            a[x] -= a[y];
        }
        if(y  < n - 1)
            sort(x, y+1);

    }

    public static void prnt(int i){
        System.out.print(a[i] + " ");
        if(i < n - 1)
            prnt(i+1);
    }

    public static void main(String[] args){
        scan(0);
        srt(0);
        prnt(0);
    }

}
