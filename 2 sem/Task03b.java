import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task03b {

    static double sum = 0;

    public static void polinom(double x, int n) {
        if (n >= 0) {
            Scanner scanner = new Scanner(System.in);
            double a = scanner.nextDouble();
            sum = sum * x + a;
            n--;
            polinom(x, n);
        } else {
            System.out.println(sum);
        }
    }

    static int step = 1;
    static int rez = 0;
    public static void converter(int a, int b){
        if (b > 0) {
            rez = rez + (b % 10) * step;
            step = step * a;
            b = b / 10;
            converter(a,b);
        }else{
            System.out.println(rez);
        }
    }

    static boolean ch = true, nech = true;

    static int kol = 0, dlin = 0, dano = 0;

    public static void uslovie(int n){
        if (n != 0 | dano != 0){
        if (dano == 0){
            Scanner scanner = new Scanner(System.in);
            dano = scanner.nextInt();
            if ((ch | nech) && (dlin == 3 | dlin == 5)) {
                kol ++;
            }
            ch = true;
            nech = true;
            dlin = 0;
            n--;
            uslovie(n);
            return;
        }
        int ad = dano % 10;
        dano = dano / 10;
        dlin ++;
        if (ad % 2 == 0) {
            nech = false;
        }else{
            ch = false;
        }
        uslovie(n);
        return;
        }
        else {
            if (kol == 2){
                System.out.println("YES");
            }else {
                System.out.println("NO");
            }
        }

    }

    static int i = 0;
    static String a2,b2;

    public static void twoStrings(String a, String b, int n){
        char a1, b1;
        a2 = a.toLowerCase();
        b2 = b.toLowerCase();
        a1 = a2.charAt(i);
        b1 = b2.charAt(i);
        if (i < n && a1 == b1) {
            if (i == n-1){
                if (a.length() == n) {
                    System.out.println(a);
                    System.out.println(b);
                }else{
                    System.out.println(b);
                    System.out.println(a);
                }
            }else {
            i++;
            twoStrings(a,b,n);}
        }else{
            if (a1 > b1) {
                System.out.println(b);
                System.out.println(a);
            } else {

                System.out.println(a);
                System.out.println(b);
            }
        }
    }

    public static void main(String[] args) {
        polinom(5, 2);
        converter(5,10);
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        String b = scanner.nextLine();
        int n;
        if (a.length() > b.length()) {
            n = b.length();
        } else {
            n = a.length();
        }
        twoStrings(a,b,n);

        uslovie(5);
    }

}
