import java.util.Scanner;

/**
 * Created by Damir on 22.02.2015.
 */
public class Task07c {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }
        while (p.getNext().getNext().getNext().getNext() != null){
            p = p.getNext();
        }
        p.setNext(null);
        p = head;
        while (p != null){
            System.out.println(p.getValue());
            p = p.getNext();
        }

    }
}
