import java.util.Scanner;

/**
 * Created by Damir on 22.02.2015.
 */
public class Task08b {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }
        while (p != null){
            if (p.getValue() % 2 == 0) {
                Elem p1 = new Elem(p.getValue(), p.getNext());
                p.setValue(100);
                p.setNext(p1);
                p = p.getNext().getNext();
            }else{
                p = p.getNext();
            }
        }
        p = head;
        while (p != null){
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
