import java.util.Scanner;

/**
 * Created by Damir on 23.02.2015.
 */
public class Task10a {
    static int dva = 1;
    public static int step(int a){
        if (a >= 1){
            dva *= 2;
            step(a-1);
        }
        return dva;
    }
    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }
        int sum = 0;
        int k = step(n-1);
        while (p.getNext() != null){
            sum += k * p.getValue();
            k = k / 2;
            p = p.getNext();
        }
        sum += p.getValue();

        head = null;
        p = null;
        while (sum != 0){
            p = new Elem(sum % 10, head);
            sum /= 10;
            head = p;
        }
        p = head;
        while (p != null){
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
