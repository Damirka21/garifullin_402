import java.util.Scanner;

/**
 * Created by Damir on 22.02.2015.
 */
public class Task09a {

    public static int first(int d){
        while (d > 9) {
            d /= 10;
        }
        return d;
    }
    public static boolean prostoe(int d) {
        boolean a;
        if ( d == 1 ){
            a = false;
        }else{
            a = true;
        }
        for ( int i = d / 2; i > 1 ; i --){
            if (d % i == 0){
                a = false;
            }
        }
        return a;
    }

    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }

        while (p != null){
            if (prostoe(p.getValue())) {
                Elem p2 = new Elem(p.getValue() % 10, p.getNext());
                Elem p1 = new Elem(p.getValue(), p2);
                p.setValue(first(p.getValue()));
                p.setNext(p1);
                p = p.getNext().getNext().getNext();
            }else{
                p = p.getNext();
            }
        }
        p = head;
        while (p != null){
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
