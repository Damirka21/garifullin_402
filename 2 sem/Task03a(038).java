import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task03aa {

    static boolean flag = true;
    static Scanner scanner = new Scanner(System.in);
    static int n = scanner.nextInt();
    static double[][] a = new double[n][n];

    public static void scan(int i,int j){
        if(flag){
            a[i][j] = scanner.nextDouble();
            if(j == n - 1){
               i ++;
               j = -1;
               if (i == n ) {
                   flag = false;
               }
            }
            scan(i,j+1);
        }
    }

    static double del ;
    static int mind;
    public static void delitel(int i, int j){
        if(i < n - 1){
            del = a[i+1][j] / a[j][j] ;
            mind = j;
            stroka(i + 1,j);
            delitel(i + 1,j);

        }else {
            j++;
            if (j<n) {
                delitel(j, j);
            }
        }
    }

    public static void stroka(int x, int y){
        a[x][y] = a[x][y]  - a[mind][y] * del;
        if(y +1 < n){
            stroka(x,y+1);
        }
    }

    public static void print(int i,int j){
        System.out.print(a[i][j] + " ");
        if(j < n - 1 ) {
            print(i,j+1);
        } else {
            System.out.println();
            if (i < n - 1){
                i ++;
                j = 0;
                print(i,j);
            }
        }
    }

    public static void main(String[] args){
        scan(0,0);
        delitel(0,0);
        //stroka(0,0);
        print(0,0);
    }

}
