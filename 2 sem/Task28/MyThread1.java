package Task28;

import Tasks23_27.MyThread;

import java.util.Calendar;

/**
 * Created by Damir on 09.05.2015.
 */
public class MyThread1 implements Runnable {

    private Thread thread;

    private int[] array;

    public void join() throws InterruptedException {
        thread.join();
    }

    public int getSum() {
        return sum;
    }

    private int sum = 0;

    public MyThread1 (int[] a){
        thread = new Thread(this);
        array = a;
        thread.start();
    }


    @Override
    public void run() {
        for (int i = 1; i < array.length; i += 2) {
            sum += array[i];
        }
    }
}
