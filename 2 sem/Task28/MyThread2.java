package Task28;

import java.util.Calendar;

/**
 * Created by Damir on 09.05.2015.
 */
public class MyThread2 implements Runnable {
    private Thread thread;

    private int[] array;

    public int getSum() {
        return sum;
    }

    private int sum = 0;

    public MyThread2 (int[] a){
        thread = new Thread(this);
        array = a;
        thread.start();
    }

    public void join() throws InterruptedException {
        thread.join();
    }

    @Override
    public void run() {
        for (int i = 0; i < array.length; i += 2) {
            sum += array[i];
        }
    }
}
