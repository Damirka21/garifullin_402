package Task28;

import java.util.Scanner;

/**
 * Created by Damir on 09.05.2015.
 */
public class Task28b {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n ; i ++){
            array[i]= scanner.nextInt();
        }
        int sum = 0;
        int[] size = new int[k +1] ;
        size[0] = 0;
        if (n % k == 0){
            for (int j = 1; j<k + 1; j ++){
                size[j] = n/k * j;
            }
        }else{
            for (int j = 1; j<k + 1; j ++){
                size[j] = n/k * j;
            }
            int additional = n%k;
            int j = 1;
            while(additional!=0){
                for (int l = 1; l<k + 1; l ++){
                    size[l] ++;

                }
                additional--;
            }
        }

        int i = 0;

        while(i < k){
            MySuperThread mst = new MySuperThread(array,size[i],size[i+1]);
            mst.join();
            i++;
            sum += mst.getSum();
        }
        System.out.println(sum);
    }
}
