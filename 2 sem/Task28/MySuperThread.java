package Task28;

/**
 * Created by Damir on 09.05.2015.
 */
public class MySuperThread implements Runnable{
    private Thread thread;

    private int[] array;
    private int from;
    private int to;

    public void join() throws InterruptedException {
        thread.join();
    }

    public int getSum() {
        return sum;
    }

    private int sum = 0;

    public MySuperThread (int[] a, int from, int to){
        thread = new Thread(this);
        array = a;
        this.from = from;
        this.to = to;
        thread.start();
    }


    @Override
    public void run() {
        for (int i = from; i < to ; i ++) {
            sum += array[i];
        }
    }
}