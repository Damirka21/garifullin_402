package Task28;

import java.util.Scanner;

/**
 * Created by Damir on 09.05.2015.
 */
public class Task28c {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for ( int i = 0; i < n; i ++){
            arr[i] = scanner.nextInt();
        }
        MyThread1 t1 = new MyThread1(arr);
        MyThread2 t2 = new MyThread2(arr);
        t1.join();
        t2.join();
        System.out.println(t2.getSum() + t1.getSum());

    }
}
