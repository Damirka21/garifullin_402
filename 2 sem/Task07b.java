import java.util.Scanner;

/**
 * Created by Damir on 22.02.2015.
 */
public class Task07b {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }
        while (p.getNext() != null){
            if (p.getNext().getValue() % 2 == 1) {
                p.setNext(p.getNext().getNext());
            }else{
                p = p.getNext();
            }
        }
        while (head.getValue() % 2 == 1){
            head = head.getNext();
        }
        p = head;
        while (p != null){
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
