import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task06a {
    public static void main(String[] args) {
        Elem head = new Elem();
        Elem p = new Elem();
        Elem next = new Elem();

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        head.setValue(scanner.nextInt());
        head.setNext(p);
        for (int i = 1; i < n; i++) {
            int d = scanner.nextInt();
            p.setValue(d);
            p.setNext(next);
            next = new Elem();
            p = p.getNext();
        }
        next = null;
         p = head;
        while(p.getNext() != null){
            System.out.println(p.getValue());
            p = p.getNext();
        }

    }
}
