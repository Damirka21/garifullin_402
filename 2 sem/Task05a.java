import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task05a {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }
        int val = p.getValue();
        p = p.getNext();
        int max = 0;
        int min = 0;
        while (p.getNext() != null) {
            if (p.getValue() > val && p.getValue() > p.getNext().getValue()){
                max ++;
            }
            if (p.getValue() < val && p.getValue() < p.getNext().getValue()){
                min ++;
            }
            val = p.getValue();
            p = p.getNext();
        }
        System.out.println(max + " " + min);
    }
}
