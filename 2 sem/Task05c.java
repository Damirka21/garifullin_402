import java.util.Scanner;

/**
 * @author Damir Garifullin
 *         11-402
 */
public class Task05c {
    public static void main(String[] args) {
        Elem head = null;
        Elem p = null;

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            int d = scanner.nextInt();
            p = new Elem(d, head);
            head = p;
        }
        int sum = 0;
        while (p != null) {
            if (p.getValue() % 2 == 0) {
                sum += p.getValue();
            }
            p = p.getNext();
        }
        System.out.println(sum);
    }
}
